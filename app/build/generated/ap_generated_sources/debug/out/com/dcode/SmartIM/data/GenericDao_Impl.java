package com.dcode.SmartIM.data;

import android.database.Cursor;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.dcode.SmartIM.data.model.BIN_ITEMS;
import com.dcode.SmartIM.data.model.BIN_STOCK;
import com.dcode.SmartIM.data.model.GENERIC_COUNT;
import com.dcode.SmartIM.data.model.USER_BIN_NOS;
import java.lang.Class;
import java.lang.Integer;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings({"unchecked", "deprecation"})
public final class GenericDao_Impl implements GenericDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<BIN_ITEMS> __insertionAdapterOfBIN_ITEMS;

  private final EntityInsertionAdapter<USER_BIN_NOS> __insertionAdapterOfUSER_BIN_NOS;

  private final SharedSQLiteStatement __preparedStmtOfDeleteBinItemByID;

  private final SharedSQLiteStatement __preparedStmtOfDeleteBinByID;

  private final SharedSQLiteStatement __preparedStmtOfDeleteBinByIDfromBinItems;

  private final SharedSQLiteStatement __preparedStmtOfDeleteAllBIN_ITEMS;

  private final SharedSQLiteStatement __preparedStmtOfUpdateBinItem;

  public GenericDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfBIN_ITEMS = new EntityInsertionAdapter<BIN_ITEMS>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `BIN_ITEMS` (`PK_ID`,`TRANS_ID`,`BIN_NO`,`TR_BIN_NO`,`BIN_MODE`,`SCAN_TIME`,`LOC_CODE`,`QTY`,`BARCODE`,`PART_NO`,`PART_NAME`,`UOM`,`STK_QTY`,`ROW_NUM`,`DEVICE_ID`,`LOGON_NAME`,`REMARKS`,`IS_NEW`,`TRANS_GUID`) VALUES (nullif(?, 0),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, BIN_ITEMS value) {
        stmt.bindLong(1, value.PK_ID);
        stmt.bindLong(2, value.TRANS_ID);
        if (value.BIN_NO == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.BIN_NO);
        }
        if (value.TR_BIN_NO == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.TR_BIN_NO);
        }
        if (value.BIN_MODE == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.BIN_MODE);
        }
        if (value.SCAN_TIME == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.SCAN_TIME);
        }
        if (value.LOC_CODE == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.LOC_CODE);
        }
        stmt.bindLong(8, value.QTY);
        if (value.BARCODE == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.BARCODE);
        }
        if (value.PART_NO == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.PART_NO);
        }
        if (value.PART_NAME == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindString(11, value.PART_NAME);
        }
        if (value.UOM == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.UOM);
        }
        stmt.bindLong(13, value.STK_QTY);
        if (value.ROW_NUM == null) {
          stmt.bindNull(14);
        } else {
          stmt.bindString(14, value.ROW_NUM);
        }
        if (value.DEVICE_ID == null) {
          stmt.bindNull(15);
        } else {
          stmt.bindString(15, value.DEVICE_ID);
        }
        if (value.LOGON_NAME == null) {
          stmt.bindNull(16);
        } else {
          stmt.bindString(16, value.LOGON_NAME);
        }
        if (value.REMARKS == null) {
          stmt.bindNull(17);
        } else {
          stmt.bindString(17, value.REMARKS);
        }
        stmt.bindLong(18, value.IS_NEW);
        if (value.TRANS_GUID == null) {
          stmt.bindNull(19);
        } else {
          stmt.bindString(19, value.TRANS_GUID);
        }
      }
    };
    this.__insertionAdapterOfUSER_BIN_NOS = new EntityInsertionAdapter<USER_BIN_NOS>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `USER_BIN_NOS` (`ID`,`BIN_NO`,`EXP_QTY`,`CR_BY`) VALUES (nullif(?, 0),?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, USER_BIN_NOS value) {
        stmt.bindLong(1, value.ID);
        if (value.BIN_NO == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.BIN_NO);
        }
        stmt.bindLong(3, value.EXP_QTY);
        stmt.bindLong(4, value.CR_BY);
      }
    };
    this.__preparedStmtOfDeleteBinItemByID = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM BIN_ITEMS WHERE PK_ID=?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteBinByID = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM USER_BIN_NOS WHERE BIN_NO=?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteBinByIDfromBinItems = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM BIN_ITEMS WHERE BIN_NO=?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteAllBIN_ITEMS = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM BIN_ITEMS ";
        return _query;
      }
    };
    this.__preparedStmtOfUpdateBinItem = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "UPDATE BIN_ITEMS SET QTY=? WHERE PK_ID=?";
        return _query;
      }
    };
  }

  @Override
  public void insertBin_Items(final BIN_ITEMS... bin_items) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfBIN_ITEMS.insert(bin_items);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void insertBin_No(final USER_BIN_NOS... bin_nos) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfUSER_BIN_NOS.insert(bin_nos);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void deleteBinItemByID(final long pk_id) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteBinItemByID.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, pk_id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteBinItemByID.release(_stmt);
    }
  }

  @Override
  public void deleteBinByID(final String binNo) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteBinByID.acquire();
    int _argIndex = 1;
    if (binNo == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, binNo);
    }
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteBinByID.release(_stmt);
    }
  }

  @Override
  public void deleteBinByIDfromBinItems(final String binNo) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteBinByIDfromBinItems.acquire();
    int _argIndex = 1;
    if (binNo == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, binNo);
    }
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteBinByIDfromBinItems.release(_stmt);
    }
  }

  @Override
  public void deleteAllBIN_ITEMS() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteAllBIN_ITEMS.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteAllBIN_ITEMS.release(_stmt);
    }
  }

  @Override
  public void updateBinItem(final String qty, final int pkId) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdateBinItem.acquire();
    int _argIndex = 1;
    if (qty == null) {
      _stmt.bindNull(_argIndex);
    } else {
      _stmt.bindString(_argIndex, qty);
    }
    _argIndex = 2;
    _stmt.bindLong(_argIndex, pkId);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdateBinItem.release(_stmt);
    }
  }

  @Override
  public List<BIN_ITEMS> getItemsByBin(final int trans_id, final String binNo) {
    final String _sql = "SELECT PK_ID, TRANS_ID, LOC_CODE, BIN_NO, BIN_MODE, SCAN_TIME, QTY, STK_QTY, BARCODE, PART_NO, PART_NAME, UOM,  TR_BIN_NO, DEVICE_ID, LOGON_NAME, REMARKS, IS_NEW, TRANS_GUID, (SELECT COUNT(*) FROM BIN_ITEMS b WHERE BIN_ITEMS.PK_ID >= b.PK_ID  AND BIN_ITEMS.BIN_NO = b.BIN_NO AND BIN_ITEMS.TRANS_ID = b.TRANS_ID) ROW_NUM FROM BIN_ITEMS WHERE TRANS_ID=? AND BIN_NO = ? ORDER BY PK_ID DESC";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 2);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, trans_id);
    _argIndex = 2;
    if (binNo == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, binNo);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfPKID = CursorUtil.getColumnIndexOrThrow(_cursor, "PK_ID");
      final int _cursorIndexOfTRANSID = CursorUtil.getColumnIndexOrThrow(_cursor, "TRANS_ID");
      final int _cursorIndexOfLOCCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "LOC_CODE");
      final int _cursorIndexOfBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_NO");
      final int _cursorIndexOfBINMODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_MODE");
      final int _cursorIndexOfSCANTIME = CursorUtil.getColumnIndexOrThrow(_cursor, "SCAN_TIME");
      final int _cursorIndexOfQTY = CursorUtil.getColumnIndexOrThrow(_cursor, "QTY");
      final int _cursorIndexOfSTKQTY = CursorUtil.getColumnIndexOrThrow(_cursor, "STK_QTY");
      final int _cursorIndexOfBARCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BARCODE");
      final int _cursorIndexOfPARTNO = CursorUtil.getColumnIndexOrThrow(_cursor, "PART_NO");
      final int _cursorIndexOfPARTNAME = CursorUtil.getColumnIndexOrThrow(_cursor, "PART_NAME");
      final int _cursorIndexOfUOM = CursorUtil.getColumnIndexOrThrow(_cursor, "UOM");
      final int _cursorIndexOfTRBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "TR_BIN_NO");
      final int _cursorIndexOfDEVICEID = CursorUtil.getColumnIndexOrThrow(_cursor, "DEVICE_ID");
      final int _cursorIndexOfLOGONNAME = CursorUtil.getColumnIndexOrThrow(_cursor, "LOGON_NAME");
      final int _cursorIndexOfREMARKS = CursorUtil.getColumnIndexOrThrow(_cursor, "REMARKS");
      final int _cursorIndexOfISNEW = CursorUtil.getColumnIndexOrThrow(_cursor, "IS_NEW");
      final int _cursorIndexOfTRANSGUID = CursorUtil.getColumnIndexOrThrow(_cursor, "TRANS_GUID");
      final int _cursorIndexOfROWNUM = CursorUtil.getColumnIndexOrThrow(_cursor, "ROW_NUM");
      final List<BIN_ITEMS> _result = new ArrayList<BIN_ITEMS>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final BIN_ITEMS _item;
        _item = new BIN_ITEMS();
        _item.PK_ID = _cursor.getInt(_cursorIndexOfPKID);
        _item.TRANS_ID = _cursor.getInt(_cursorIndexOfTRANSID);
        if (_cursor.isNull(_cursorIndexOfLOCCODE)) {
          _item.LOC_CODE = null;
        } else {
          _item.LOC_CODE = _cursor.getString(_cursorIndexOfLOCCODE);
        }
        if (_cursor.isNull(_cursorIndexOfBINNO)) {
          _item.BIN_NO = null;
        } else {
          _item.BIN_NO = _cursor.getString(_cursorIndexOfBINNO);
        }
        if (_cursor.isNull(_cursorIndexOfBINMODE)) {
          _item.BIN_MODE = null;
        } else {
          _item.BIN_MODE = _cursor.getString(_cursorIndexOfBINMODE);
        }
        if (_cursor.isNull(_cursorIndexOfSCANTIME)) {
          _item.SCAN_TIME = null;
        } else {
          _item.SCAN_TIME = _cursor.getString(_cursorIndexOfSCANTIME);
        }
        _item.QTY = _cursor.getLong(_cursorIndexOfQTY);
        _item.STK_QTY = _cursor.getLong(_cursorIndexOfSTKQTY);
        if (_cursor.isNull(_cursorIndexOfBARCODE)) {
          _item.BARCODE = null;
        } else {
          _item.BARCODE = _cursor.getString(_cursorIndexOfBARCODE);
        }
        if (_cursor.isNull(_cursorIndexOfPARTNO)) {
          _item.PART_NO = null;
        } else {
          _item.PART_NO = _cursor.getString(_cursorIndexOfPARTNO);
        }
        if (_cursor.isNull(_cursorIndexOfPARTNAME)) {
          _item.PART_NAME = null;
        } else {
          _item.PART_NAME = _cursor.getString(_cursorIndexOfPARTNAME);
        }
        if (_cursor.isNull(_cursorIndexOfUOM)) {
          _item.UOM = null;
        } else {
          _item.UOM = _cursor.getString(_cursorIndexOfUOM);
        }
        if (_cursor.isNull(_cursorIndexOfTRBINNO)) {
          _item.TR_BIN_NO = null;
        } else {
          _item.TR_BIN_NO = _cursor.getString(_cursorIndexOfTRBINNO);
        }
        if (_cursor.isNull(_cursorIndexOfDEVICEID)) {
          _item.DEVICE_ID = null;
        } else {
          _item.DEVICE_ID = _cursor.getString(_cursorIndexOfDEVICEID);
        }
        if (_cursor.isNull(_cursorIndexOfLOGONNAME)) {
          _item.LOGON_NAME = null;
        } else {
          _item.LOGON_NAME = _cursor.getString(_cursorIndexOfLOGONNAME);
        }
        if (_cursor.isNull(_cursorIndexOfREMARKS)) {
          _item.REMARKS = null;
        } else {
          _item.REMARKS = _cursor.getString(_cursorIndexOfREMARKS);
        }
        _item.IS_NEW = _cursor.getInt(_cursorIndexOfISNEW);
        if (_cursor.isNull(_cursorIndexOfTRANSGUID)) {
          _item.TRANS_GUID = null;
        } else {
          _item.TRANS_GUID = _cursor.getString(_cursorIndexOfTRANSGUID);
        }
        if (_cursor.isNull(_cursorIndexOfROWNUM)) {
          _item.ROW_NUM = null;
        } else {
          _item.ROW_NUM = _cursor.getString(_cursorIndexOfROWNUM);
        }
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<BIN_ITEMS> getAllBIN_ITEMS() {
    final String _sql = "SELECT PK_ID,QTY,TRANS_ID,BARCODE,PART_NAME,BIN_MODE,PART_NO,STK_QTY, DEVICE_ID, BIN_NO, TR_BIN_NO, SCAN_TIME, LOC_CODE, UOM, ROW_NUM, LOGON_NAME, REMARKS,IS_NEW, TRANS_GUID FROM  BIN_ITEMS ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfPKID = CursorUtil.getColumnIndexOrThrow(_cursor, "PK_ID");
      final int _cursorIndexOfQTY = CursorUtil.getColumnIndexOrThrow(_cursor, "QTY");
      final int _cursorIndexOfTRANSID = CursorUtil.getColumnIndexOrThrow(_cursor, "TRANS_ID");
      final int _cursorIndexOfBARCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BARCODE");
      final int _cursorIndexOfPARTNAME = CursorUtil.getColumnIndexOrThrow(_cursor, "PART_NAME");
      final int _cursorIndexOfBINMODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_MODE");
      final int _cursorIndexOfPARTNO = CursorUtil.getColumnIndexOrThrow(_cursor, "PART_NO");
      final int _cursorIndexOfSTKQTY = CursorUtil.getColumnIndexOrThrow(_cursor, "STK_QTY");
      final int _cursorIndexOfDEVICEID = CursorUtil.getColumnIndexOrThrow(_cursor, "DEVICE_ID");
      final int _cursorIndexOfBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_NO");
      final int _cursorIndexOfTRBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "TR_BIN_NO");
      final int _cursorIndexOfSCANTIME = CursorUtil.getColumnIndexOrThrow(_cursor, "SCAN_TIME");
      final int _cursorIndexOfLOCCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "LOC_CODE");
      final int _cursorIndexOfUOM = CursorUtil.getColumnIndexOrThrow(_cursor, "UOM");
      final int _cursorIndexOfROWNUM = CursorUtil.getColumnIndexOrThrow(_cursor, "ROW_NUM");
      final int _cursorIndexOfLOGONNAME = CursorUtil.getColumnIndexOrThrow(_cursor, "LOGON_NAME");
      final int _cursorIndexOfREMARKS = CursorUtil.getColumnIndexOrThrow(_cursor, "REMARKS");
      final int _cursorIndexOfISNEW = CursorUtil.getColumnIndexOrThrow(_cursor, "IS_NEW");
      final int _cursorIndexOfTRANSGUID = CursorUtil.getColumnIndexOrThrow(_cursor, "TRANS_GUID");
      final List<BIN_ITEMS> _result = new ArrayList<BIN_ITEMS>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final BIN_ITEMS _item;
        _item = new BIN_ITEMS();
        _item.PK_ID = _cursor.getInt(_cursorIndexOfPKID);
        _item.QTY = _cursor.getLong(_cursorIndexOfQTY);
        _item.TRANS_ID = _cursor.getInt(_cursorIndexOfTRANSID);
        if (_cursor.isNull(_cursorIndexOfBARCODE)) {
          _item.BARCODE = null;
        } else {
          _item.BARCODE = _cursor.getString(_cursorIndexOfBARCODE);
        }
        if (_cursor.isNull(_cursorIndexOfPARTNAME)) {
          _item.PART_NAME = null;
        } else {
          _item.PART_NAME = _cursor.getString(_cursorIndexOfPARTNAME);
        }
        if (_cursor.isNull(_cursorIndexOfBINMODE)) {
          _item.BIN_MODE = null;
        } else {
          _item.BIN_MODE = _cursor.getString(_cursorIndexOfBINMODE);
        }
        if (_cursor.isNull(_cursorIndexOfPARTNO)) {
          _item.PART_NO = null;
        } else {
          _item.PART_NO = _cursor.getString(_cursorIndexOfPARTNO);
        }
        _item.STK_QTY = _cursor.getLong(_cursorIndexOfSTKQTY);
        if (_cursor.isNull(_cursorIndexOfDEVICEID)) {
          _item.DEVICE_ID = null;
        } else {
          _item.DEVICE_ID = _cursor.getString(_cursorIndexOfDEVICEID);
        }
        if (_cursor.isNull(_cursorIndexOfBINNO)) {
          _item.BIN_NO = null;
        } else {
          _item.BIN_NO = _cursor.getString(_cursorIndexOfBINNO);
        }
        if (_cursor.isNull(_cursorIndexOfTRBINNO)) {
          _item.TR_BIN_NO = null;
        } else {
          _item.TR_BIN_NO = _cursor.getString(_cursorIndexOfTRBINNO);
        }
        if (_cursor.isNull(_cursorIndexOfSCANTIME)) {
          _item.SCAN_TIME = null;
        } else {
          _item.SCAN_TIME = _cursor.getString(_cursorIndexOfSCANTIME);
        }
        if (_cursor.isNull(_cursorIndexOfLOCCODE)) {
          _item.LOC_CODE = null;
        } else {
          _item.LOC_CODE = _cursor.getString(_cursorIndexOfLOCCODE);
        }
        if (_cursor.isNull(_cursorIndexOfUOM)) {
          _item.UOM = null;
        } else {
          _item.UOM = _cursor.getString(_cursorIndexOfUOM);
        }
        if (_cursor.isNull(_cursorIndexOfROWNUM)) {
          _item.ROW_NUM = null;
        } else {
          _item.ROW_NUM = _cursor.getString(_cursorIndexOfROWNUM);
        }
        if (_cursor.isNull(_cursorIndexOfLOGONNAME)) {
          _item.LOGON_NAME = null;
        } else {
          _item.LOGON_NAME = _cursor.getString(_cursorIndexOfLOGONNAME);
        }
        if (_cursor.isNull(_cursorIndexOfREMARKS)) {
          _item.REMARKS = null;
        } else {
          _item.REMARKS = _cursor.getString(_cursorIndexOfREMARKS);
        }
        _item.IS_NEW = _cursor.getInt(_cursorIndexOfISNEW);
        if (_cursor.isNull(_cursorIndexOfTRANSGUID)) {
          _item.TRANS_GUID = null;
        } else {
          _item.TRANS_GUID = _cursor.getString(_cursorIndexOfTRANSGUID);
        }
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<BIN_ITEMS> getLast20BIN_ITEMS() {
    final String _sql = "SELECT PK_ID,QTY,TRANS_ID,BARCODE,PART_NAME,BIN_MODE,PART_NO,STK_QTY, DEVICE_ID, BIN_NO, TR_BIN_NO, SCAN_TIME, LOC_CODE, UOM, ROW_NUM, LOGON_NAME, REMARKS,IS_NEW, TRANS_GUID FROM  BIN_ITEMS ORDER BY PK_ID DESC LIMIT 20";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfPKID = CursorUtil.getColumnIndexOrThrow(_cursor, "PK_ID");
      final int _cursorIndexOfQTY = CursorUtil.getColumnIndexOrThrow(_cursor, "QTY");
      final int _cursorIndexOfTRANSID = CursorUtil.getColumnIndexOrThrow(_cursor, "TRANS_ID");
      final int _cursorIndexOfBARCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BARCODE");
      final int _cursorIndexOfPARTNAME = CursorUtil.getColumnIndexOrThrow(_cursor, "PART_NAME");
      final int _cursorIndexOfBINMODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_MODE");
      final int _cursorIndexOfPARTNO = CursorUtil.getColumnIndexOrThrow(_cursor, "PART_NO");
      final int _cursorIndexOfSTKQTY = CursorUtil.getColumnIndexOrThrow(_cursor, "STK_QTY");
      final int _cursorIndexOfDEVICEID = CursorUtil.getColumnIndexOrThrow(_cursor, "DEVICE_ID");
      final int _cursorIndexOfBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_NO");
      final int _cursorIndexOfTRBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "TR_BIN_NO");
      final int _cursorIndexOfSCANTIME = CursorUtil.getColumnIndexOrThrow(_cursor, "SCAN_TIME");
      final int _cursorIndexOfLOCCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "LOC_CODE");
      final int _cursorIndexOfUOM = CursorUtil.getColumnIndexOrThrow(_cursor, "UOM");
      final int _cursorIndexOfROWNUM = CursorUtil.getColumnIndexOrThrow(_cursor, "ROW_NUM");
      final int _cursorIndexOfLOGONNAME = CursorUtil.getColumnIndexOrThrow(_cursor, "LOGON_NAME");
      final int _cursorIndexOfREMARKS = CursorUtil.getColumnIndexOrThrow(_cursor, "REMARKS");
      final int _cursorIndexOfISNEW = CursorUtil.getColumnIndexOrThrow(_cursor, "IS_NEW");
      final int _cursorIndexOfTRANSGUID = CursorUtil.getColumnIndexOrThrow(_cursor, "TRANS_GUID");
      final List<BIN_ITEMS> _result = new ArrayList<BIN_ITEMS>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final BIN_ITEMS _item;
        _item = new BIN_ITEMS();
        _item.PK_ID = _cursor.getInt(_cursorIndexOfPKID);
        _item.QTY = _cursor.getLong(_cursorIndexOfQTY);
        _item.TRANS_ID = _cursor.getInt(_cursorIndexOfTRANSID);
        if (_cursor.isNull(_cursorIndexOfBARCODE)) {
          _item.BARCODE = null;
        } else {
          _item.BARCODE = _cursor.getString(_cursorIndexOfBARCODE);
        }
        if (_cursor.isNull(_cursorIndexOfPARTNAME)) {
          _item.PART_NAME = null;
        } else {
          _item.PART_NAME = _cursor.getString(_cursorIndexOfPARTNAME);
        }
        if (_cursor.isNull(_cursorIndexOfBINMODE)) {
          _item.BIN_MODE = null;
        } else {
          _item.BIN_MODE = _cursor.getString(_cursorIndexOfBINMODE);
        }
        if (_cursor.isNull(_cursorIndexOfPARTNO)) {
          _item.PART_NO = null;
        } else {
          _item.PART_NO = _cursor.getString(_cursorIndexOfPARTNO);
        }
        _item.STK_QTY = _cursor.getLong(_cursorIndexOfSTKQTY);
        if (_cursor.isNull(_cursorIndexOfDEVICEID)) {
          _item.DEVICE_ID = null;
        } else {
          _item.DEVICE_ID = _cursor.getString(_cursorIndexOfDEVICEID);
        }
        if (_cursor.isNull(_cursorIndexOfBINNO)) {
          _item.BIN_NO = null;
        } else {
          _item.BIN_NO = _cursor.getString(_cursorIndexOfBINNO);
        }
        if (_cursor.isNull(_cursorIndexOfTRBINNO)) {
          _item.TR_BIN_NO = null;
        } else {
          _item.TR_BIN_NO = _cursor.getString(_cursorIndexOfTRBINNO);
        }
        if (_cursor.isNull(_cursorIndexOfSCANTIME)) {
          _item.SCAN_TIME = null;
        } else {
          _item.SCAN_TIME = _cursor.getString(_cursorIndexOfSCANTIME);
        }
        if (_cursor.isNull(_cursorIndexOfLOCCODE)) {
          _item.LOC_CODE = null;
        } else {
          _item.LOC_CODE = _cursor.getString(_cursorIndexOfLOCCODE);
        }
        if (_cursor.isNull(_cursorIndexOfUOM)) {
          _item.UOM = null;
        } else {
          _item.UOM = _cursor.getString(_cursorIndexOfUOM);
        }
        if (_cursor.isNull(_cursorIndexOfROWNUM)) {
          _item.ROW_NUM = null;
        } else {
          _item.ROW_NUM = _cursor.getString(_cursorIndexOfROWNUM);
        }
        if (_cursor.isNull(_cursorIndexOfLOGONNAME)) {
          _item.LOGON_NAME = null;
        } else {
          _item.LOGON_NAME = _cursor.getString(_cursorIndexOfLOGONNAME);
        }
        if (_cursor.isNull(_cursorIndexOfREMARKS)) {
          _item.REMARKS = null;
        } else {
          _item.REMARKS = _cursor.getString(_cursorIndexOfREMARKS);
        }
        _item.IS_NEW = _cursor.getInt(_cursorIndexOfISNEW);
        if (_cursor.isNull(_cursorIndexOfTRANSGUID)) {
          _item.TRANS_GUID = null;
        } else {
          _item.TRANS_GUID = _cursor.getString(_cursorIndexOfTRANSGUID);
        }
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<USER_BIN_NOS> getUserBinNos() {
    final String _sql = "SELECT DISTINCT ID,BIN_NO,EXP_QTY,CR_BY FROM  USER_BIN_NOS ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfID = CursorUtil.getColumnIndexOrThrow(_cursor, "ID");
      final int _cursorIndexOfBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_NO");
      final int _cursorIndexOfEXPQTY = CursorUtil.getColumnIndexOrThrow(_cursor, "EXP_QTY");
      final int _cursorIndexOfCRBY = CursorUtil.getColumnIndexOrThrow(_cursor, "CR_BY");
      final List<USER_BIN_NOS> _result = new ArrayList<USER_BIN_NOS>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final USER_BIN_NOS _item;
        _item = new USER_BIN_NOS();
        _item.ID = _cursor.getInt(_cursorIndexOfID);
        if (_cursor.isNull(_cursorIndexOfBINNO)) {
          _item.BIN_NO = null;
        } else {
          _item.BIN_NO = _cursor.getString(_cursorIndexOfBINNO);
        }
        _item.EXP_QTY = _cursor.getInt(_cursorIndexOfEXPQTY);
        _item.CR_BY = _cursor.getInt(_cursorIndexOfCRBY);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<BIN_STOCK> getUserBinStockCount() {
    final String _sql = "Select SUM(ITEM_COUNT) ITEM_COUNT,BIN_NO, SUM(EXP_QTY) EXP_QTY FROM (SELECT SUM(QTY) ITEM_COUNT,BIN_ITEMS.BIN_NO BIN_NO,0 EXP_QTY FROM BIN_ITEMS left join USER_BIN_NOS ON BIN_ITEMS.BIN_NO=USER_BIN_NOS.BIN_NO GROUP BY BIN_ITEMS.BIN_NO UNION SELECT 0 ITEM_COUNT,USER_BIN_NOS.BIN_NO BIN_NO,MAX(EXP_QTY) EXP_QTY FROM USER_BIN_NOS left join BIN_ITEMS ON BIN_ITEMS.BIN_NO=USER_BIN_NOS.BIN_NO GROUP BY USER_BIN_NOS.BIN_NO ) GROUP BY BIN_NO ORDER BY BIN_NO ";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfITEMCOUNT = CursorUtil.getColumnIndexOrThrow(_cursor, "ITEM_COUNT");
      final int _cursorIndexOfBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_NO");
      final int _cursorIndexOfEXPQTY = CursorUtil.getColumnIndexOrThrow(_cursor, "EXP_QTY");
      final List<BIN_STOCK> _result = new ArrayList<BIN_STOCK>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final BIN_STOCK _item;
        _item = new BIN_STOCK();
        _item.ITEM_COUNT = _cursor.getInt(_cursorIndexOfITEMCOUNT);
        if (_cursor.isNull(_cursorIndexOfBINNO)) {
          _item.BIN_NO = null;
        } else {
          _item.BIN_NO = _cursor.getString(_cursorIndexOfBINNO);
        }
        _item.EXP_QTY = _cursor.getInt(_cursorIndexOfEXPQTY);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<String> getBinNos() {
    final String _sql = "SELECT DISTINCT  BIN_NO FROM  USER_BIN_NOS";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final List<String> _result = new ArrayList<String>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final String _item;
        if (_cursor.isNull(0)) {
          _item = null;
        } else {
          _item = _cursor.getString(0);
        }
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public Integer getCount(final String binNo) {
    final String _sql = "SELECT COUNT(*) FROM BIN_ITEMS WHERE BIN_NO=?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (binNo == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, binNo);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final Integer _result;
      if(_cursor.moveToFirst()) {
        final Integer _tmp;
        if (_cursor.isNull(0)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getInt(0);
        }
        _result = _tmp;
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<BIN_ITEMS> getBinItems(final String binNo) {
    final String _sql = "SELECT DISTINCT * FROM BIN_ITEMS WHERE BIN_NO=?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (binNo == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, binNo);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfPKID = CursorUtil.getColumnIndexOrThrow(_cursor, "PK_ID");
      final int _cursorIndexOfTRANSID = CursorUtil.getColumnIndexOrThrow(_cursor, "TRANS_ID");
      final int _cursorIndexOfBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_NO");
      final int _cursorIndexOfTRBINNO = CursorUtil.getColumnIndexOrThrow(_cursor, "TR_BIN_NO");
      final int _cursorIndexOfBINMODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BIN_MODE");
      final int _cursorIndexOfSCANTIME = CursorUtil.getColumnIndexOrThrow(_cursor, "SCAN_TIME");
      final int _cursorIndexOfLOCCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "LOC_CODE");
      final int _cursorIndexOfQTY = CursorUtil.getColumnIndexOrThrow(_cursor, "QTY");
      final int _cursorIndexOfBARCODE = CursorUtil.getColumnIndexOrThrow(_cursor, "BARCODE");
      final int _cursorIndexOfPARTNO = CursorUtil.getColumnIndexOrThrow(_cursor, "PART_NO");
      final int _cursorIndexOfPARTNAME = CursorUtil.getColumnIndexOrThrow(_cursor, "PART_NAME");
      final int _cursorIndexOfUOM = CursorUtil.getColumnIndexOrThrow(_cursor, "UOM");
      final int _cursorIndexOfSTKQTY = CursorUtil.getColumnIndexOrThrow(_cursor, "STK_QTY");
      final int _cursorIndexOfROWNUM = CursorUtil.getColumnIndexOrThrow(_cursor, "ROW_NUM");
      final int _cursorIndexOfDEVICEID = CursorUtil.getColumnIndexOrThrow(_cursor, "DEVICE_ID");
      final int _cursorIndexOfLOGONNAME = CursorUtil.getColumnIndexOrThrow(_cursor, "LOGON_NAME");
      final int _cursorIndexOfREMARKS = CursorUtil.getColumnIndexOrThrow(_cursor, "REMARKS");
      final int _cursorIndexOfISNEW = CursorUtil.getColumnIndexOrThrow(_cursor, "IS_NEW");
      final int _cursorIndexOfTRANSGUID = CursorUtil.getColumnIndexOrThrow(_cursor, "TRANS_GUID");
      final List<BIN_ITEMS> _result = new ArrayList<BIN_ITEMS>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final BIN_ITEMS _item;
        _item = new BIN_ITEMS();
        _item.PK_ID = _cursor.getInt(_cursorIndexOfPKID);
        _item.TRANS_ID = _cursor.getInt(_cursorIndexOfTRANSID);
        if (_cursor.isNull(_cursorIndexOfBINNO)) {
          _item.BIN_NO = null;
        } else {
          _item.BIN_NO = _cursor.getString(_cursorIndexOfBINNO);
        }
        if (_cursor.isNull(_cursorIndexOfTRBINNO)) {
          _item.TR_BIN_NO = null;
        } else {
          _item.TR_BIN_NO = _cursor.getString(_cursorIndexOfTRBINNO);
        }
        if (_cursor.isNull(_cursorIndexOfBINMODE)) {
          _item.BIN_MODE = null;
        } else {
          _item.BIN_MODE = _cursor.getString(_cursorIndexOfBINMODE);
        }
        if (_cursor.isNull(_cursorIndexOfSCANTIME)) {
          _item.SCAN_TIME = null;
        } else {
          _item.SCAN_TIME = _cursor.getString(_cursorIndexOfSCANTIME);
        }
        if (_cursor.isNull(_cursorIndexOfLOCCODE)) {
          _item.LOC_CODE = null;
        } else {
          _item.LOC_CODE = _cursor.getString(_cursorIndexOfLOCCODE);
        }
        _item.QTY = _cursor.getLong(_cursorIndexOfQTY);
        if (_cursor.isNull(_cursorIndexOfBARCODE)) {
          _item.BARCODE = null;
        } else {
          _item.BARCODE = _cursor.getString(_cursorIndexOfBARCODE);
        }
        if (_cursor.isNull(_cursorIndexOfPARTNO)) {
          _item.PART_NO = null;
        } else {
          _item.PART_NO = _cursor.getString(_cursorIndexOfPARTNO);
        }
        if (_cursor.isNull(_cursorIndexOfPARTNAME)) {
          _item.PART_NAME = null;
        } else {
          _item.PART_NAME = _cursor.getString(_cursorIndexOfPARTNAME);
        }
        if (_cursor.isNull(_cursorIndexOfUOM)) {
          _item.UOM = null;
        } else {
          _item.UOM = _cursor.getString(_cursorIndexOfUOM);
        }
        _item.STK_QTY = _cursor.getLong(_cursorIndexOfSTKQTY);
        if (_cursor.isNull(_cursorIndexOfROWNUM)) {
          _item.ROW_NUM = null;
        } else {
          _item.ROW_NUM = _cursor.getString(_cursorIndexOfROWNUM);
        }
        if (_cursor.isNull(_cursorIndexOfDEVICEID)) {
          _item.DEVICE_ID = null;
        } else {
          _item.DEVICE_ID = _cursor.getString(_cursorIndexOfDEVICEID);
        }
        if (_cursor.isNull(_cursorIndexOfLOGONNAME)) {
          _item.LOGON_NAME = null;
        } else {
          _item.LOGON_NAME = _cursor.getString(_cursorIndexOfLOGONNAME);
        }
        if (_cursor.isNull(_cursorIndexOfREMARKS)) {
          _item.REMARKS = null;
        } else {
          _item.REMARKS = _cursor.getString(_cursorIndexOfREMARKS);
        }
        _item.IS_NEW = _cursor.getInt(_cursorIndexOfISNEW);
        if (_cursor.isNull(_cursorIndexOfTRANSGUID)) {
          _item.TRANS_GUID = null;
        } else {
          _item.TRANS_GUID = _cursor.getString(_cursorIndexOfTRANSGUID);
        }
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public String getQty() {
    final String _sql = "SELECT SUM(QTY) FROM BIN_ITEMS";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final String _result;
      if(_cursor.moveToFirst()) {
        final String _tmp;
        if (_cursor.isNull(0)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getString(0);
        }
        _result = _tmp;
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public GENERIC_COUNT getCount(final SupportSQLiteQuery query) {
    final SupportSQLiteQuery _internalQuery = query;
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _internalQuery, false, null);
    try {
      final int _cursorIndexOfTOTALCOUNT = CursorUtil.getColumnIndex(_cursor, "TOTAL_COUNT");
      final GENERIC_COUNT _result;
      if(_cursor.moveToFirst()) {
        _result = new GENERIC_COUNT();
        if (_cursorIndexOfTOTALCOUNT != -1) {
          _result.TOTAL_COUNT = _cursor.getInt(_cursorIndexOfTOTALCOUNT);
        }
      } else {
        _result = null;
      }
      return _result;
    } finally {
      _cursor.close();
    }
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
