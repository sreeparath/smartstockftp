package com.dcode.SmartIM.data;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.Class;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class AppDatabase_Impl extends AppDatabase {
  private volatile GenericDao _genericDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(1) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `TRANS_HDR` (`PK_ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `DEVICE_ID` TEXT, `MODULE_ID` INTEGER NOT NULL, `IS_GENERATED` INTEGER NOT NULL, `IS_CLOSED` INTEGER NOT NULL, `STORE_CODE` TEXT, `USER_NAME` TEXT, `TRANS_NO` TEXT, `DEST_LOC` TEXT DEFAULT '', `TRANS_TYPE` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `BIN_ITEMS` (`PK_ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `TRANS_ID` INTEGER NOT NULL, `BIN_NO` TEXT, `TR_BIN_NO` TEXT, `BIN_MODE` TEXT, `SCAN_TIME` TEXT, `LOC_CODE` TEXT, `QTY` INTEGER NOT NULL, `BARCODE` TEXT, `PART_NO` TEXT, `PART_NAME` TEXT, `UOM` TEXT, `STK_QTY` INTEGER NOT NULL, `ROW_NUM` TEXT DEFAULT '', `DEVICE_ID` TEXT DEFAULT '', `LOGON_NAME` TEXT DEFAULT '', `REMARKS` TEXT, `IS_NEW` INTEGER NOT NULL, `TRANS_GUID` TEXT)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `ITEMMAST` (`PART_NO` TEXT NOT NULL, `PART_NAME` TEXT, `UOM` TEXT, `BARCODE` TEXT, PRIMARY KEY(`PART_NO`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `USER_BIN_NOS` (`ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `BIN_NO` TEXT, `EXP_QTY` INTEGER NOT NULL, `CR_BY` INTEGER NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'bf8cb3d6092065498f4edc91682e07a8')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `TRANS_HDR`");
        _db.execSQL("DROP TABLE IF EXISTS `BIN_ITEMS`");
        _db.execSQL("DROP TABLE IF EXISTS `ITEMMAST`");
        _db.execSQL("DROP TABLE IF EXISTS `USER_BIN_NOS`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsTRANSHDR = new HashMap<String, TableInfo.Column>(10);
        _columnsTRANSHDR.put("PK_ID", new TableInfo.Column("PK_ID", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTRANSHDR.put("DEVICE_ID", new TableInfo.Column("DEVICE_ID", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTRANSHDR.put("MODULE_ID", new TableInfo.Column("MODULE_ID", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTRANSHDR.put("IS_GENERATED", new TableInfo.Column("IS_GENERATED", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTRANSHDR.put("IS_CLOSED", new TableInfo.Column("IS_CLOSED", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTRANSHDR.put("STORE_CODE", new TableInfo.Column("STORE_CODE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTRANSHDR.put("USER_NAME", new TableInfo.Column("USER_NAME", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTRANSHDR.put("TRANS_NO", new TableInfo.Column("TRANS_NO", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsTRANSHDR.put("DEST_LOC", new TableInfo.Column("DEST_LOC", "TEXT", false, 0, "''", TableInfo.CREATED_FROM_ENTITY));
        _columnsTRANSHDR.put("TRANS_TYPE", new TableInfo.Column("TRANS_TYPE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysTRANSHDR = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesTRANSHDR = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoTRANSHDR = new TableInfo("TRANS_HDR", _columnsTRANSHDR, _foreignKeysTRANSHDR, _indicesTRANSHDR);
        final TableInfo _existingTRANSHDR = TableInfo.read(_db, "TRANS_HDR");
        if (! _infoTRANSHDR.equals(_existingTRANSHDR)) {
          return new RoomOpenHelper.ValidationResult(false, "TRANS_HDR(com.dcode.SmartIM.data.model.TRANS_HDR).\n"
                  + " Expected:\n" + _infoTRANSHDR + "\n"
                  + " Found:\n" + _existingTRANSHDR);
        }
        final HashMap<String, TableInfo.Column> _columnsBINITEMS = new HashMap<String, TableInfo.Column>(19);
        _columnsBINITEMS.put("PK_ID", new TableInfo.Column("PK_ID", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("TRANS_ID", new TableInfo.Column("TRANS_ID", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("BIN_NO", new TableInfo.Column("BIN_NO", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("TR_BIN_NO", new TableInfo.Column("TR_BIN_NO", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("BIN_MODE", new TableInfo.Column("BIN_MODE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("SCAN_TIME", new TableInfo.Column("SCAN_TIME", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("LOC_CODE", new TableInfo.Column("LOC_CODE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("QTY", new TableInfo.Column("QTY", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("BARCODE", new TableInfo.Column("BARCODE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("PART_NO", new TableInfo.Column("PART_NO", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("PART_NAME", new TableInfo.Column("PART_NAME", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("UOM", new TableInfo.Column("UOM", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("STK_QTY", new TableInfo.Column("STK_QTY", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("ROW_NUM", new TableInfo.Column("ROW_NUM", "TEXT", false, 0, "''", TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("DEVICE_ID", new TableInfo.Column("DEVICE_ID", "TEXT", false, 0, "''", TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("LOGON_NAME", new TableInfo.Column("LOGON_NAME", "TEXT", false, 0, "''", TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("REMARKS", new TableInfo.Column("REMARKS", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("IS_NEW", new TableInfo.Column("IS_NEW", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsBINITEMS.put("TRANS_GUID", new TableInfo.Column("TRANS_GUID", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysBINITEMS = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesBINITEMS = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoBINITEMS = new TableInfo("BIN_ITEMS", _columnsBINITEMS, _foreignKeysBINITEMS, _indicesBINITEMS);
        final TableInfo _existingBINITEMS = TableInfo.read(_db, "BIN_ITEMS");
        if (! _infoBINITEMS.equals(_existingBINITEMS)) {
          return new RoomOpenHelper.ValidationResult(false, "BIN_ITEMS(com.dcode.SmartIM.data.model.BIN_ITEMS).\n"
                  + " Expected:\n" + _infoBINITEMS + "\n"
                  + " Found:\n" + _existingBINITEMS);
        }
        final HashMap<String, TableInfo.Column> _columnsITEMMAST = new HashMap<String, TableInfo.Column>(4);
        _columnsITEMMAST.put("PART_NO", new TableInfo.Column("PART_NO", "TEXT", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsITEMMAST.put("PART_NAME", new TableInfo.Column("PART_NAME", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsITEMMAST.put("UOM", new TableInfo.Column("UOM", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsITEMMAST.put("BARCODE", new TableInfo.Column("BARCODE", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysITEMMAST = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesITEMMAST = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoITEMMAST = new TableInfo("ITEMMAST", _columnsITEMMAST, _foreignKeysITEMMAST, _indicesITEMMAST);
        final TableInfo _existingITEMMAST = TableInfo.read(_db, "ITEMMAST");
        if (! _infoITEMMAST.equals(_existingITEMMAST)) {
          return new RoomOpenHelper.ValidationResult(false, "ITEMMAST(com.dcode.SmartIM.data.model.ITEMMAST).\n"
                  + " Expected:\n" + _infoITEMMAST + "\n"
                  + " Found:\n" + _existingITEMMAST);
        }
        final HashMap<String, TableInfo.Column> _columnsUSERBINNOS = new HashMap<String, TableInfo.Column>(4);
        _columnsUSERBINNOS.put("ID", new TableInfo.Column("ID", "INTEGER", true, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsUSERBINNOS.put("BIN_NO", new TableInfo.Column("BIN_NO", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsUSERBINNOS.put("EXP_QTY", new TableInfo.Column("EXP_QTY", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsUSERBINNOS.put("CR_BY", new TableInfo.Column("CR_BY", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysUSERBINNOS = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesUSERBINNOS = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoUSERBINNOS = new TableInfo("USER_BIN_NOS", _columnsUSERBINNOS, _foreignKeysUSERBINNOS, _indicesUSERBINNOS);
        final TableInfo _existingUSERBINNOS = TableInfo.read(_db, "USER_BIN_NOS");
        if (! _infoUSERBINNOS.equals(_existingUSERBINNOS)) {
          return new RoomOpenHelper.ValidationResult(false, "USER_BIN_NOS(com.dcode.SmartIM.data.model.USER_BIN_NOS).\n"
                  + " Expected:\n" + _infoUSERBINNOS + "\n"
                  + " Found:\n" + _existingUSERBINNOS);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "bf8cb3d6092065498f4edc91682e07a8", "bab979450eca56e10c27787241c53708");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "TRANS_HDR","BIN_ITEMS","ITEMMAST","USER_BIN_NOS");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `TRANS_HDR`");
      _db.execSQL("DELETE FROM `BIN_ITEMS`");
      _db.execSQL("DELETE FROM `ITEMMAST`");
      _db.execSQL("DELETE FROM `USER_BIN_NOS`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  protected Map<Class<?>, List<Class<?>>> getRequiredTypeConverters() {
    final HashMap<Class<?>, List<Class<?>>> _typeConvertersMap = new HashMap<Class<?>, List<Class<?>>>();
    _typeConvertersMap.put(GenericDao.class, GenericDao_Impl.getRequiredConverters());
    return _typeConvertersMap;
  }

  @Override
  public GenericDao genericDao() {
    if (_genericDao != null) {
      return _genericDao;
    } else {
      synchronized(this) {
        if(_genericDao == null) {
          _genericDao = new GenericDao_Impl(this);
        }
        return _genericDao;
      }
    }
  }
}
