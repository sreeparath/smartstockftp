package com.dcode.SmartIM.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.SmartIM.App;
import com.dcode.SmartIM.R;
import com.dcode.SmartIM.data.MastersDBManager;
import com.dcode.SmartIM.data.model.BIN_ITEMS;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class BinItemDetailsAdapter extends RecyclerView.Adapter<BinItemDetailsAdapter.ViewHolder> {
    private List<BIN_ITEMS> listItems;
    AlertDialog alertDialog;
    private MastersDBManager mastersDBManager;
    private BinStockAdapter viewAdapter;

    public BinItemDetailsAdapter(ArrayList<BIN_ITEMS> list) {
        listItems = list;
        setHasStableIds(true);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_bin_stock_edit_row, parent, false));
    }

    public void getBinStockData(String binNo) {
        //this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getItemsByBin();
        this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getBinItems(binNo);
//        this.binItemsList=App.getDatabaseClient().getAppDatabase().genericDao().getCount();
//        notifyItemRangeChanged(0, listItems.size());
        notifyDataSetChanged();
//        if (listItems.isEmpty()) {
//            return false;
//        } else
//            return true;
    }

    public void removeItem(int position) {
        BIN_ITEMS item = listItems.get(position);
        App.getDatabaseClient().getAppDatabase().genericDao().deleteBinItemByID(item.PK_ID);
        listItems.remove(item);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return this.listItems.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        BIN_ITEMS items = listItems.get(position);
        holder.txtFirst.setText(items.BARCODE);
        holder.binNo.setText(items.BIN_NO);
        holder.txtSecond.setText(items.PART_NO);
        holder.txtThird.setText(items.PART_NAME);
        holder.txtFourth.setText(items.BIN_MODE);
        holder.txtFifth.setText(String.valueOf(items.QTY));
        holder.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                App.getDatabaseClient().getAppDatabase().genericDao().updateBinItem(holder.txtFifth.getText().toString(), items.PK_ID);
                notifyDataSetChanged();
                getBinStockData(items.BIN_NO);
                Toast.makeText(v.getContext(),"SAVED",Toast.LENGTH_LONG).show();
            }

//                    else{
//
//                    androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
//                    AlertDialog alertDialog = builder.setTitle("Invalid").setMessage("Barcode entered is invalid").setPositiveButton("OK", (dialog, which) -> {
////                        if (isCancellable) {
////                            finish();
////                        }
//                }).create();
//                alertDialog.setCancelable(true);
//                alertDialog.show();
//                }
//                }
//            }catch (Exception e){
//                Log.d(App.TAG, e.toString());
////            showAlert("Error", e.getMessage());
//            }
//            }

        });

//        holder.itemView.setTag(items);
//        holder.itemView.setOnClickListener(shortClickListener);

    }

//    private void OnSaveClick(String barcode, String id) {
//        try{
//
//            ITEM_MASTER items = mastersDBManager.getFtpStockMastByBarcode(barcode);
//            App.getDatabaseClient().getAppDatabase().genericDao().deleteBinByID(id);
//            AlertDialog.Builder alert = new AlertDialog.Builder();
//            alert.setCancelable(false);
//            alert.setTitle("Delete Bin ?");
//            notifyDataSetChanged();
//
//        }catch (Exception e){
//            Log.d(App.TAG, e.toString());
////            showAlert("Error", e.getMessage());
//        }
//    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtFirst;
        TextView txtSecond;
        TextView txtThird;
        TextView txtFourth;
        TextView txtFifth;
        TextView binNo;
        MaterialButton btnSave;

        public ViewHolder(View view) {
            super(view);
            txtFirst = view.findViewById(R.id.TextFirst);
            binNo = view.findViewById(R.id.BinNo);
            txtSecond = view.findViewById(R.id.TextSecond);
            txtThird = view.findViewById(R.id.TextThird);
            txtFourth = view.findViewById(R.id.TextFourth);
            txtFifth = view.findViewById(R.id.TextFifth);
            btnSave = view.findViewById(R.id.btnSave);
        }
    }
}
