package com.dcode.SmartIM.adapter;

import android.app.Fragment;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.SmartIM.App;
import com.dcode.SmartIM.R;
import com.dcode.SmartIM.data.model.BIN_STOCK;


import com.dcode.SmartIM.ui.view.BinStockFragment;
import com.dcode.SmartIM.ui.view.MainActivity;
import com.dcode.SmartIM.ui.view.binStockEditActivity;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class BinStockAdapter extends RecyclerView.Adapter<BinStockAdapter.ViewHolder> {

    //    private final View.OnClickListener shortClickListener;

    private List<BIN_STOCK> listItems;

    public BinStockAdapter(ArrayList<BIN_STOCK> list) {
//        this.shortClickListener = shortClickListener;
        listItems = list;

        setHasStableIds(true);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_bin_stock_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final BIN_STOCK items = listItems.get(position);
        holder.setIsRecyclable(false);

//         binItems.add(binItemsList.get());

        holder.binNo.setText(items.BIN_NO);
        holder.quantity.setText(String.valueOf(items.EXP_QTY));
//
//        if(binItems.isEmpty()){
//            holder.count.setText(String.valueOf(0));
//        }
//
//        else{
//            holder.count.setText(binItems.get(position).toString());}

        holder.count.setText(String.valueOf(items.ITEM_COUNT));

        holder.itemView.setTag(items);
        holder.btnBinEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), binStockEditActivity.class);
                intent.putExtra("binNo",items.BIN_NO);
                v.getContext().startActivity(intent);

//                {
//                    super.onActivityResult(requestCode, resultCode, data);
//                    if ((requestCode == 10001) && (resultCode == Activity.RESULT_OK))
//                        // recreate your fragment here
//                        FragmentTransaction ft = getFragmentManager().beginTransaction();
//                    ft.detach(frag).attach(frag).commit();
//                }
            }

        });
//binItems.get(0).toString()
//        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public int getItemCount() {
        return this.listItems.size();
    }

    //    public Boolean getBinData() {
//        //this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getItemsByBin();
//        this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getUserBinNos();
////        this.binItemsList=App.getDatabaseClient().getAppDatabase().genericDao().getCount();
//        notifyItemRangeChanged(0, listItems.size());
//        notifyDataSetChanged();
//        if (listItems.isEmpty()) {
//            return false;
//        } else
//            return true;
//    }
    public Boolean getBinStockData() {
        //this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getItemsByBin();
        this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getUserBinStockCount();
//        this.binItemsList=App.getDatabaseClient().getAppDatabase().genericDao().getCount();
        notifyItemRangeChanged(0, listItems.size());
        notifyDataSetChanged();
        if (listItems.isEmpty()) {
            return false;
        } else
            return true;
    }


//    public void refreshData() {
//        //this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getItemsByBin();
//        this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getUserBinNos();
//        notifyItemRangeChanged(0, listItems.size());
//        notifyDataSetChanged();
//    }

    public void removeItem(int position) {
        BIN_STOCK item = listItems.get(position);
        App.getDatabaseClient().getAppDatabase().genericDao().deleteBinByIDfromBinItems(item.BIN_NO);
        listItems.remove(item);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }




    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView binNo;
        TextView quantity;
        TextView count;
        MaterialButton btnBinEdit;


        public ViewHolder(View view) {
            super(view);
            binNo = view.findViewById(R.id.tvBinNo);
            quantity = view.findViewById(R.id.tvQuantity);
            count = view.findViewById(R.id.tvCount);
            btnBinEdit = view.findViewById(R.id.btnViewBinItems);
//            btnBinEdit.setOnClickListener(v -> OnBinEditClick());

        }

//        private void OnBinEditClick() {
//            ((MainActivity) requireActivity()).NavigateToFragment(R.id.binStockEditFragment,null);
////            mNavController.navigate(R.id.binStockEditFragment);
//        }
    }
}


