package com.dcode.SmartIM.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.SmartIM.App;
import com.dcode.SmartIM.R;
import com.dcode.SmartIM.data.model.BIN_ITEMS;

import java.util.ArrayList;
import java.util.List;

public class ListViewAdapter
        extends RecyclerView.Adapter<ListViewAdapter.ViewHolder> {
    private final View.OnClickListener shortClickListener;
    private List<BIN_ITEMS> listItems;

    public ListViewAdapter(ArrayList<BIN_ITEMS> list,
                           View.OnClickListener shortClickListener) {
        this.shortClickListener = shortClickListener;
        listItems = list;
        setHasStableIds(true);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_item_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final BIN_ITEMS items = listItems.get(position);
        holder.txtFirst.setText(items.BARCODE);
        holder.binNo.setText(items.BIN_NO);
        holder.txtSecond.setText(items.PART_NO);
        holder.txtThird.setText(items.PART_NAME);
        holder.txtFourth.setText(items.BIN_MODE);
        holder.txtFifth.setText(String.valueOf(items.QTY));

        holder.itemView.setTag(items);
        holder.itemView.setOnClickListener(shortClickListener);
    }

    @Override
    public long getItemId(int position) {
        return listItems.get(position).PK_ID;
    }

    @Override
    public int getItemCount() {
        return this.listItems.size();
    }

    public BIN_ITEMS getItemByPosition(int position) {
        return this.listItems.get(position);
    }

    public void refreshData(int trans_id, String binNo) {
        this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getItemsByBin(trans_id, binNo);
        notifyItemRangeChanged(0, listItems.size());
    }

    public void refreshData() {
        //this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getItemsByBin();
        this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getLast20BIN_ITEMS();
        notifyItemRangeChanged(0, listItems.size());
    }

    public void clearData() {
        this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getItemsByBin(-1, "");
        notifyDataSetChanged();
    }

    public void clearList() {
        this.listItems = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        BIN_ITEMS item = listItems.get(position);
        App.getDatabaseClient().getAppDatabase().genericDao().deleteBinItemByID(item.PK_ID);
        listItems.remove(item);
        notifyItemRemoved(position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtFirst;
        TextView txtSecond;
        TextView txtThird;
        TextView txtFourth;
        TextView txtFifth;
        TextView binNo;

        public ViewHolder(View view) {
            super(view);
            txtFirst = view.findViewById(R.id.TextFirst);
            binNo=view.findViewById(R.id.BinNo);
            txtSecond = view.findViewById(R.id.TextSecond);
            txtThird = view.findViewById(R.id.TextThird);
            txtFourth = view.findViewById(R.id.TextFourth);
            txtFifth = view.findViewById(R.id.TextFifth);
        }
    }
}
