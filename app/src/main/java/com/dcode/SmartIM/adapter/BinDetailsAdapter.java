package com.dcode.SmartIM.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.SmartIM.App;
import com.dcode.SmartIM.R;
import com.dcode.SmartIM.data.model.USER_BIN_NOS;

import java.util.ArrayList;
import java.util.List;

public class BinDetailsAdapter extends RecyclerView.Adapter<BinDetailsAdapter.ViewHolder>{
    private List<USER_BIN_NOS> listItems;
    public BinDetailsAdapter(ArrayList<USER_BIN_NOS> list) {
//        this.shortClickListener = shortClickListener;
        listItems = list;
        setHasStableIds(true);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_bin_detail_row, parent, false));
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final USER_BIN_NOS items = listItems.get(position);
        holder.setIsRecyclable(false);
        holder.binNo.setText(items.BIN_NO);
        holder.quantity.setText(String.valueOf(items.EXP_QTY));
        holder.itemView.setTag(items);
    }
    @Override
    public int getItemCount() {
        return this.listItems.size();
    }
    public Boolean getBinData() {
        //this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getItemsByBin();
        this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getUserBinNos();
        notifyItemRangeChanged(0, listItems.size());
        notifyDataSetChanged();
        if (listItems.isEmpty()) {
            return false;
        } else
            return true;
    }

    public void refreshData() {
        //this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getItemsByBin();
        this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getUserBinNos();
        notifyItemRangeChanged(0, listItems.size());
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        USER_BIN_NOS item = listItems.get(position);
        App.getDatabaseClient().getAppDatabase().genericDao().deleteBinByID(item.BIN_NO);
        listItems.remove(item);
        notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView binNo;
        TextView quantity;
        TextView count;

        public ViewHolder(View view) {
            super(view);
            binNo = view.findViewById(R.id.tvBinNo);
            quantity = view.findViewById(R.id.tvQuantity);
            count= view.findViewById(R.id.tvCount);

        }
    }
}
