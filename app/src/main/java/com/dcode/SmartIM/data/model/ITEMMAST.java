package com.dcode.SmartIM.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/*
CREATE TABLE ITEMMAST (
    PART_NO   TEXT,
    PART_NAME TEXT,
    UOM       TEXT,
    BARCODE   TEXT
);
*/

@Entity(tableName = "ITEMMAST")
public class ITEMMAST {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "PART_NO")
    public String PART_NO = "";

    @ColumnInfo(name = "PART_NAME")
    public String PART_NAME = "";

    @ColumnInfo(name = "UOM")
    public String UOM = "";

    @ColumnInfo(name = "BARCODE")
    public String BARCODE = "";

    public String CodeLine() {
        return String.format("%s %s %s %s", PART_NO, PART_NAME, UOM, BARCODE);
    }

    public ITEMMAST GetItem() {
        String strEmpty = "";
        ITEMMAST itemmast = new ITEMMAST();
        itemmast.PART_NO = strEmpty;
        itemmast.PART_NAME = strEmpty;
        itemmast.UOM = strEmpty;
        itemmast.BARCODE = strEmpty;
        return itemmast;
    }
}
