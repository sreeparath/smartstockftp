package com.dcode.SmartIM.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "FTP_SETTINGS")
public class FTP_SETTINGS {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID")
    public long ID;


    @ColumnInfo(name = "HOST")
    public String HOST = "";

    @ColumnInfo(name = "PORT")
    public int PORT = 0;

    @ColumnInfo(name = "USER_NAME")
    public String USER_NAME = "";

    @ColumnInfo(name = "PASSWORD")
    public String PASSWORD = "";

    @ColumnInfo(name = "TARGET_DIR")
    public String TARGET_DIR = "";
}
