package com.dcode.SmartIM.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "USERS")
public class USERS {
    @PrimaryKey
    @ColumnInfo(name = "ID")
    public int ID;

    @ColumnInfo(name = "LOGIN_NAME")
    public String LOGIN_NAME;

    @ColumnInfo(name = "PASSWD")
    public String PASSWD;

    @ColumnInfo(name = "IS_PASSWORD_EXPIRES")
    public int IS_PASSWORD_EXPIRES;

    @ColumnInfo(name = "PASSWORD_EXPIRY_DATE")
    public String PASSWORD_EXPIRY_DATE;

    @ColumnInfo(name = "ALLOW_QTY_OVERRIDE")
    public int ALLOW_QTY_OVERRIDE;

    @ColumnInfo(name = "ALLOW_BIN_CREATION")
    public int ALLOW_BIN_CREATION;
}
