package com.dcode.SmartIM.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.dcode.SmartIM.common.Utils;

@Entity(tableName = "BIN_ITEMS")
public class BIN_ITEMS {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "PK_ID")
    public int PK_ID;

    @ColumnInfo(name = "TRANS_ID")
    public int TRANS_ID;

    @ColumnInfo(name = "BIN_NO")
    public String BIN_NO;

    @ColumnInfo(name = "TR_BIN_NO")
    public String TR_BIN_NO;

    @ColumnInfo(name = "BIN_MODE")
    public String BIN_MODE;

    @ColumnInfo(name = "SCAN_TIME")
    public String SCAN_TIME;

    @ColumnInfo(name = "LOC_CODE")
    public String LOC_CODE;

    @ColumnInfo(name = "QTY")
    public long QTY;

    @ColumnInfo(name = "BARCODE")
    public String BARCODE = "";

    @ColumnInfo(name = "PART_NO")
    public String PART_NO = "";

    @ColumnInfo(name = "PART_NAME")
    public String PART_NAME = "";

    @ColumnInfo(name = "UOM")
    public String UOM = "";

    @ColumnInfo(name = "STK_QTY")
    public long STK_QTY;

    @ColumnInfo(name = "ROW_NUM", defaultValue = "")
    public String ROW_NUM = "";

    @ColumnInfo(name = "DEVICE_ID", defaultValue = "")
    public String DEVICE_ID = "";

    @ColumnInfo(name = "LOGON_NAME", defaultValue = "")
    public String LOGON_NAME = "";

    @ColumnInfo(name = "REMARKS")
    public String REMARKS = "";

    @ColumnInfo(name = "IS_NEW")
    public int IS_NEW = 0;

    @ColumnInfo(name = "TRANS_GUID")
    public String TRANS_GUID = Utils.GetGUID();

    @Override
    public String toString() {
//        return String.format("%s,%s,%s,%s,%s,%s,%s", TRANS_ID, BARCODE, PART_NAME, BIN_MODE, PART_NO, STK_QTY, DEVICE_ID);
        //barcode, user_id, loc_code, item_desc ?? if input by user else blank, NewItem (0/1),item_line_guid, qty, <scan timestamp_yyyyMMddHHmmss>
        return String.format("%s,%s,%s,%s,%s,%s,%d,%s", BARCODE, LOGON_NAME, LOC_CODE, IS_NEW == 1 ? PART_NAME : "", IS_NEW, TRANS_GUID, STK_QTY, SCAN_TIME);

    }


}