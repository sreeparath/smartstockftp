package com.dcode.SmartIM.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = "STORES")
public class STORES {
    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "STORE_CODE")
    public String STORE_CODE;

    @ColumnInfo(name = "STORE_NAME")
    public String STORE_NAME;

    @ColumnInfo(name = "VALIDATION_TYPE")
    public String VALIDATION_TYPE;

    @Override
    public String toString() {
        return STORE_NAME;
    }
}
