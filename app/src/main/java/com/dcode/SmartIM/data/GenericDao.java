package com.dcode.SmartIM.data;

import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.sqlite.db.SupportSQLiteQuery;

import com.dcode.SmartIM.data.model.BIN_ITEMS;
import com.dcode.SmartIM.data.model.BIN_STOCK;
import com.dcode.SmartIM.data.model.GENERIC_COUNT;
import com.dcode.SmartIM.data.model.USER_BIN_NOS;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface GenericDao {

    @RawQuery
    GENERIC_COUNT getCount(SupportSQLiteQuery query);

    @Insert(onConflict = REPLACE)
    void insertBin_Items(BIN_ITEMS... bin_items);


    @Insert(onConflict = REPLACE)
    void insertBin_No(USER_BIN_NOS... bin_nos);

    @Query("DELETE FROM BIN_ITEMS WHERE PK_ID=:pk_id")
    void deleteBinItemByID(long pk_id);

    @Query("SELECT PK_ID, TRANS_ID, LOC_CODE, BIN_NO, BIN_MODE, SCAN_TIME, QTY, STK_QTY, BARCODE, " +
            "PART_NO, PART_NAME, UOM,  TR_BIN_NO, DEVICE_ID, LOGON_NAME, REMARKS, IS_NEW, TRANS_GUID, " +
            "(SELECT COUNT(*) FROM BIN_ITEMS b WHERE BIN_ITEMS.PK_ID >= b.PK_ID " +
            " AND BIN_ITEMS.BIN_NO = b.BIN_NO AND BIN_ITEMS.TRANS_ID = b.TRANS_ID) ROW_NUM " +
            "FROM BIN_ITEMS WHERE TRANS_ID=:trans_id AND BIN_NO = :binNo ORDER BY PK_ID DESC")
    List<BIN_ITEMS> getItemsByBin(int trans_id, String binNo);

    @Query("SELECT PK_ID,QTY,TRANS_ID,BARCODE,PART_NAME,BIN_MODE,PART_NO,STK_QTY, DEVICE_ID, " +
            "BIN_NO, TR_BIN_NO, SCAN_TIME, LOC_CODE, UOM, ROW_NUM, LOGON_NAME, REMARKS," +
            "IS_NEW, TRANS_GUID FROM  BIN_ITEMS ")
    List<BIN_ITEMS> getAllBIN_ITEMS();

    @Query("SELECT PK_ID,QTY,TRANS_ID,BARCODE,PART_NAME,BIN_MODE,PART_NO,STK_QTY, DEVICE_ID, " +
            "BIN_NO, TR_BIN_NO, SCAN_TIME, LOC_CODE, UOM, ROW_NUM, LOGON_NAME, REMARKS," +
            "IS_NEW, TRANS_GUID FROM  BIN_ITEMS ORDER BY PK_ID DESC LIMIT 20")
    List<BIN_ITEMS> getLast20BIN_ITEMS();

    @Query("SELECT DISTINCT ID,BIN_NO,EXP_QTY,CR_BY FROM  USER_BIN_NOS ")
    List<USER_BIN_NOS> getUserBinNos();

//    @Query("SELECT COUNT(*) ITEM_COUNT,BIN_ITEMS.BIN_NO,EXP_QTY FROM  BIN_ITEMS LEFT JOIN USER_BIN_NOS ON BIN_ITEMS.BIN_NO=USER_BIN_NOS.BIN_NO GROUP BY BIN_ITEMS.BIN_NO")
//    List<BIN_STOCK> getUserBinStockCount();

//    @Query("SELECT SUM(QTY) ITEM_COUNT,BIN_ITEMS.BIN_NO,EXP_QTY FROM  BIN_ITEMS left join USER_BIN_NOS ON BIN_ITEMS.BIN_NO=USER_BIN_NOS.BIN_NO GROUP BY BIN_ITEMS.BIN_NO" +
//            " UNION SELECT SUM(EXP_QTY) ITEM_COUNT,USER_BIN_NOS.BIN_NO,EXP_QTY FROM   USER_BIN_NOS left join  BIN_ITEMS ON BIN_ITEMS.BIN_NO=USER_BIN_NOS.BIN_NO GROUP BY USER_BIN_NOS.BIN_NO")
//    List<BIN_STOCK> getUserBinStockCount();

    @Query("Select SUM(ITEM_COUNT) ITEM_COUNT,BIN_NO, SUM(EXP_QTY) EXP_QTY FROM (SELECT SUM(QTY) ITEM_COUNT,BIN_ITEMS.BIN_NO BIN_NO,0 EXP_QTY FROM BIN_ITEMS left join USER_BIN_NOS ON BIN_ITEMS.BIN_NO=USER_BIN_NOS.BIN_NO GROUP BY BIN_ITEMS.BIN_NO UNION SELECT 0 ITEM_COUNT,USER_BIN_NOS.BIN_NO BIN_NO,MAX(EXP_QTY) EXP_QTY FROM USER_BIN_NOS left join BIN_ITEMS ON BIN_ITEMS.BIN_NO=USER_BIN_NOS.BIN_NO GROUP BY USER_BIN_NOS.BIN_NO ) GROUP BY BIN_NO ORDER BY BIN_NO ")
    List<BIN_STOCK> getUserBinStockCount();


//    @Query("SELECT DISTINCT  BIN_NO FROM  BIN_ITEMS")
//    List<String> getBinNos();

    @Query("SELECT DISTINCT  BIN_NO FROM  USER_BIN_NOS")
    List<String> getBinNos();

    @Query("DELETE FROM USER_BIN_NOS WHERE BIN_NO=:binNo")
    void deleteBinByID(String binNo);

    @Query("DELETE FROM BIN_ITEMS WHERE BIN_NO=:binNo")
    void deleteBinByIDfromBinItems(String binNo);


    @Query("SELECT COUNT(*) FROM BIN_ITEMS WHERE BIN_NO=:binNo")
    Integer getCount(String binNo);


    @Query("DELETE FROM BIN_ITEMS ")
    void deleteAllBIN_ITEMS();

    @Query("SELECT DISTINCT * FROM BIN_ITEMS WHERE BIN_NO=:binNo")
    List<BIN_ITEMS> getBinItems(String binNo);

    @Query("UPDATE BIN_ITEMS SET QTY=:qty WHERE PK_ID=:pkId")
    void updateBinItem(String qty, int pkId);

    @Query("SELECT SUM(QTY) FROM BIN_ITEMS")
    String getQty();
}