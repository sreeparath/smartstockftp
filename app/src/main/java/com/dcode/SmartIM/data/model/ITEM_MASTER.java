package com.dcode.SmartIM.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "ITEM_MASTER")
public class ITEM_MASTER {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID")
    public int ID;


    @ColumnInfo(name = "ITEM_DESC")
    public String ITEM_DESC = "";

    @ColumnInfo(name = "ITEM_CODE")
    public String ITEM_CODE = "";

    @ColumnInfo(name = "BARCODE")
    public String BARCODE = "";

    @ColumnInfo(name = "DEPT_CODE")
    public String DEPT_CODE = "";

    @ColumnInfo(name = "STYLE_CODE")
    public String STYLE_CODE = "";

    @ColumnInfo(name = "UOM")
    public String UOM = "";

    @ColumnInfo(name = "ERP_STOCK")
    public int ERP_STOCK = 0;

    @ColumnInfo(name = "REMARKS")
    public String REMARKS = "";


//    public String CodeLine() {
//        return String.format("%s %s %s %s, %d", PART_NO, PART_NAME, UOM, BARCODE, QTY);
//    }

//    public ITEM_MASTER GetItem() {
//        String strEmpty = "";
//        ITEM_MASTER stockmast = new ITEM_MASTER();
//        stockmast.PART_NO = strEmpty;
//        stockmast.PART_NAME = strEmpty;
//        stockmast.UOM = strEmpty;
//        stockmast.BARCODE = strEmpty;
//        stockmast.QTY = 0;
//        return stockmast;
//    }
}
