package com.dcode.SmartIM.data.model;

import java.io.Serializable;

public class DOC_SUMMARY implements Serializable {

    public int PK_ID;
    public int MODULE_ID;
    public String TRANS_NO;
    public int BIN_COUNT;
    public int ITEM_COUNT;
    public int IS_CHECKED;
}
