package com.dcode.SmartIM.data.model;

import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

public class USER_SETTINGS {

    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "ID")
    public int ID;

    @ColumnInfo(name = "LOGIN_NAME")
    public String LOGIN_NAME;

    @ColumnInfo(name = "USER_NAME")
    public String USER_NAME;

    @ColumnInfo(name = "USER_FULL_NAME")
    public String USER_FULL_NAME;

    @ColumnInfo(name = "USER_PASSWORD")
    public String USER_PASSWORD;

    @ColumnInfo(name = "IS_PASSWORD_EXPIRES")
    public int IS_PASSWORD_EXPIRES;

    @ColumnInfo(name = "PASSWORD_EXPIRY_DATE")
    public String PASSWORD_EXPIRY_DATE;

    @ColumnInfo(name = "ALLOW_QTY_OVERRIDE")
    public int ALLOW_QTY_OVERRIDE;

    @ColumnInfo(name = "ALLOW_BIN_CREATION")
    public int ALLOW_BIN_CREATION;

}
