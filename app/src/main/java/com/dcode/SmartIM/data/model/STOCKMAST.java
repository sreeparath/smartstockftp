package com.dcode.SmartIM.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/*
CREATE TABLE STOCKMAST (
    PART_NO   TEXT,
    PART_NAME TEXT,
    UOM       TEXT,
    BARCODE   TEXT
);
*/

@Entity(tableName = "ITEMMAST")
public class STOCKMAST {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "PART_NO")
    public String PART_NO = "";

    @ColumnInfo(name = "PART_NAME")
    public String PART_NAME = "";

    @ColumnInfo(name = "UOM")
    public String UOM = "";

    @ColumnInfo(name = "BARCODE")
    public String BARCODE = "";

    @ColumnInfo(name = "QTY")
    public int QTY = 0;

    public String CodeLine() {
        return String.format("%s %s %s %s, %d", PART_NO, PART_NAME, UOM, BARCODE, QTY);
    }

    public STOCKMAST GetItem() {
        String strEmpty = "";
        STOCKMAST stockmast = new STOCKMAST();
        stockmast.PART_NO = strEmpty;
        stockmast.PART_NAME = strEmpty;
        stockmast.UOM = strEmpty;
        stockmast.BARCODE = strEmpty;
        stockmast.QTY = 0;
        return stockmast;
    }
}
