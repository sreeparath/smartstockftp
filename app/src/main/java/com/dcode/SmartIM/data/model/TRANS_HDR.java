package com.dcode.SmartIM.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "TRANS_HDR")
public class TRANS_HDR implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "PK_ID")
    public int PK_ID;

    @ColumnInfo(name = "DEVICE_ID")
    public String DEVICE_ID;

    /* ref Navigation::Module_ID / AppConstants => 101...202*/
    @ColumnInfo(name = "MODULE_ID")
    public int MODULE_ID;

    @ColumnInfo(name = "IS_GENERATED")
    public int IS_GENERATED;

    @ColumnInfo(name = "IS_CLOSED")
    public int IS_CLOSED;

    @ColumnInfo(name = "STORE_CODE")
    public String STORE_CODE;

    @ColumnInfo(name = "USER_NAME")
    public String USER_NAME;

    @ColumnInfo(name = "TRANS_NO")
    public String TRANS_NO;

    @ColumnInfo(name = "DEST_LOC", defaultValue = "")
    public String DEST_LOC;

    @ColumnInfo(name = "TRANS_TYPE")
    public String TRANS_TYPE;
}
