package com.dcode.SmartIM.data;

import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.dcode.SmartIM.data.model.FTP_SETTINGS;
import com.dcode.SmartIM.data.model.ITEM_MASTER;
import com.dcode.SmartIM.data.model.STOCK_SETTINGS;
import com.dcode.SmartIM.data.model.USERS;
import com.dcode.SmartIM.data.model.USER_SETTINGS;

public class MastersDBManager {

    private SQLiteDatabase mDatabase;
    private final String db_file;

    public MastersDBManager(String masters_db) {
        db_file = masters_db;
    }

    public void open() throws SQLException {
        if (mDatabase == null || !mDatabase.isOpen()) {
            mDatabase = SQLiteDatabase.openDatabase(db_file, null, SQLiteDatabase.OPEN_READONLY);
        }
    }

    public void close() {
        if (mDatabase != null && mDatabase.isOpen()) {
            mDatabase.close();
            mDatabase = null;
        }
    }

/*    public List<STORES> getStores() {
        List<STORES> storesList = new ArrayList<>();
        try {
            String sqlQuery = "select * from STORES";
            Cursor cursor = mDatabase.rawQuery(sqlQuery, null);

            STORES stores;
            if (cursor != null && cursor.getCount() > 0) {
                for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                    stores = new STORES();
                    stores.STORE_CODE = cursor.getString(cursor.getColumnIndex("STORE_CODE"));
                    stores.STORE_NAME = cursor.getString(cursor.getColumnIndex("STORE_NAME"));
                    stores.VALIDATION_TYPE = cursor.getString(cursor.getColumnIndex("VALIDATION_TYPE"));
                    storesList.add(stores);
                }
                cursor.close();
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
        return storesList;
    }*/

/*    public STORES getStore(String code) {
        STORES stores = null;
        try {
            String sqlQuery = String.format("select * from STORES WHERE STORE_CODE = '%s'", code);
            Cursor cursor = mDatabase.rawQuery(sqlQuery, null);

            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                stores = new STORES();
                stores.STORE_CODE = cursor.getString(cursor.getColumnIndex("STORE_CODE"));
                stores.STORE_NAME = cursor.getString(cursor.getColumnIndex("STORE_NAME"));
                stores.VALIDATION_TYPE = cursor.getString(cursor.getColumnIndex("VALIDATION_TYPE"));
                cursor.close();
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
        return stores;
    }*/

    public USERS getUser(String login_name, String passwd) {
        String sqlTemplate = "SELECT * FROM T01_USERS WHERE LOGIN_NAME = '%s' AND USER_PASSWORD = '%s'";
        Cursor cursor = mDatabase.rawQuery(String.format(sqlTemplate, login_name, passwd), null);

        USERS users = null;
        if (cursor != null && cursor.getCount() > 0) {
            users = new USERS();
            cursor.moveToFirst();
            users.ID = cursor.getInt(cursor.getColumnIndex("ID"));
            users.LOGIN_NAME = cursor.getString(cursor.getColumnIndex("LOGIN_NAME"));
            users.PASSWD = cursor.getString(cursor.getColumnIndex("USER_PASSWORD"));

            users.IS_PASSWORD_EXPIRES = cursor.getInt(cursor.getColumnIndex("IS_PASSWORD_EXPIRES"));
            users.PASSWORD_EXPIRY_DATE = cursor.getString(cursor.getColumnIndex("PASSWORD_EXPIRY_DATE"));
            users.ALLOW_QTY_OVERRIDE = cursor.getInt(cursor.getColumnIndex("ALLOW_QTY_OVERRIDE"));
            users.ALLOW_BIN_CREATION = cursor.getInt(cursor.getColumnIndex("ALLOW_BIN_CREATION"));
        }
        return users;
    }

    public STOCK_SETTINGS getStockSettings() {
        String sqlTemplate = "SELECT * FROM STOCK_CNTS";
        Cursor cursor = mDatabase.rawQuery(String.format(sqlTemplate), null);

        STOCK_SETTINGS stock_settings = null;
        if (cursor != null && cursor.getCount() > 0) {
            stock_settings = new STOCK_SETTINGS();
            cursor.moveToFirst();
            stock_settings.ID = cursor.getInt(cursor.getColumnIndex("ID"));
            stock_settings.ERP_TYPE = cursor.getInt(cursor.getColumnIndex("ERP_TYPE"));
            stock_settings.STK_CODE = cursor.getString(cursor.getColumnIndex("STK_CODE"));
            stock_settings.DESCR = cursor.getString(cursor.getColumnIndex("DESCR"));
            stock_settings.SCH_START_DATE = cursor.getString(cursor.getColumnIndex("SCH_START_DATE"));
            stock_settings.SCH_END_DATE = cursor.getString(cursor.getColumnIndex("SCH_END_DATE"));
            stock_settings.ALLOW_NEW_BCODE = cursor.getInt(cursor.getColumnIndex("ALLOW_NEW_BCODE"));
            stock_settings.REM_MAND_FOR_NEWBCODE = cursor.getInt(cursor.getColumnIndex("REM_MAND_FOR_NEWBCODE"));
            stock_settings.LOCK_QTY = cursor.getInt(cursor.getColumnIndex("LOCK_QTY"));
            stock_settings.ORCL_STORE = cursor.getString(cursor.getColumnIndex("ORCL_STORE"));
            stock_settings.ORCL_STK_ID = cursor.getString(cursor.getColumnIndex("ORCL_STK_ID"));
            stock_settings.BIN_FROM_LIST = cursor.getInt(cursor.getColumnIndex("BIN_FROM_LIST"));
            //users.USER_GROUP = cursor.getString(cursor.getColumnIndex("USER_GROUP"));
            cursor.close();
        }
        return stock_settings;
    }

    public USER_SETTINGS getUserSettings() {
        String sqlTemplate = "SELECT * FROM T01_USERS";
        Cursor cursor = mDatabase.rawQuery(String.format(sqlTemplate), null);


        USER_SETTINGS user_settings=null;
        if (cursor != null && cursor.getCount() > 0) {
            user_settings = new USER_SETTINGS();
            cursor.moveToFirst();
            user_settings.ID = cursor.getInt(cursor.getColumnIndex("ID"));
            user_settings.LOGIN_NAME = cursor.getString(cursor.getColumnIndex("LOGIN_NAME"));
            user_settings.USER_NAME = cursor.getString(cursor.getColumnIndex("USER_NAME"));
            user_settings.USER_FULL_NAME = cursor.getString(cursor.getColumnIndex("USER_FULL_NAME"));
            user_settings.USER_PASSWORD  = cursor.getString(cursor.getColumnIndex("USER_PASSWORD "));
            user_settings.IS_PASSWORD_EXPIRES = cursor.getInt(cursor.getColumnIndex("IS_PASSWORD_EXPIRES"));
            user_settings.PASSWORD_EXPIRY_DATE = cursor.getString(cursor.getColumnIndex("PASSWORD_EXPIRY_DATE"));
            user_settings.ALLOW_QTY_OVERRIDE = cursor.getInt(cursor.getColumnIndex("ALLOW_QTY_OVERRIDE"));
            user_settings.ALLOW_BIN_CREATION = cursor.getInt(cursor.getColumnIndex("ALLOW_BIN_CREATION"));
            cursor.close();
        }
        return user_settings;
    }


    public FTP_SETTINGS getFtpSettings() {
        String sqlTemplate = "SELECT * FROM FTP_SETTINGS";
        Cursor cursor = mDatabase.rawQuery(String.format(sqlTemplate), null);

        FTP_SETTINGS ftpSettings = null;
        if (cursor != null && cursor.getCount() > 0) {
            ftpSettings = new FTP_SETTINGS();
            cursor.moveToFirst();
            ftpSettings.ID = cursor.getLong(cursor.getColumnIndex("ID"));
            ftpSettings.HOST = cursor.getString(cursor.getColumnIndex("HOST"));
            ftpSettings.PORT = cursor.getInt(cursor.getColumnIndex("PORT"));
            ftpSettings.USER_NAME = cursor.getString(cursor.getColumnIndex("USER_NAME"));
            ftpSettings.PASSWORD = cursor.getString(cursor.getColumnIndex("PASSWORD"));
            ftpSettings.TARGET_DIR = cursor.getString(cursor.getColumnIndex("TARGET_DIR"));
            cursor.close();
        }
        return ftpSettings;
    }

    public ITEM_MASTER getFtpStockMastByBarcode(String barcode) {
        String sqlTemplate = "select * from ITEM_MAST where BARCODE = '%s' ";
        String actualQuery = String.format(sqlTemplate, barcode);
        Cursor cursor = mDatabase.rawQuery(actualQuery, null);

        ITEM_MASTER itemMaster = null;

        if (cursor != null && cursor.getCount() > 0) {
            itemMaster = new ITEM_MASTER();
            cursor.moveToFirst();
            itemMaster.ID = cursor.getInt(cursor.getColumnIndex("ID"));
            itemMaster.BARCODE = cursor.getString(cursor.getColumnIndex("BARCODE"));
            itemMaster.UOM = cursor.getString(cursor.getColumnIndex("UOM"));
            itemMaster.ITEM_CODE = cursor.getString(cursor.getColumnIndex("ITEM_CODE"));
            itemMaster.ITEM_DESC = cursor.getString(cursor.getColumnIndex("ITEM_DESC"));
            itemMaster.DEPT_CODE = cursor.getString(cursor.getColumnIndex("DEPT_CODE"));
            itemMaster.STYLE_CODE = cursor.getString(cursor.getColumnIndex("STYLE_CODE"));
            itemMaster.ERP_STOCK = cursor.getInt(cursor.getColumnIndex("ERP_STOCK"));
            cursor.close();
        }

        return itemMaster;
    }
}