package com.dcode.SmartIM.data.model;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;


public class ST_LINE_ITEMS {

    public String BIN_NO = "";
    public String TR_BIN_NO = "";
    public String BARCODE = "";
    public String PART_NO = "";
    public String PART_NAME = "";
    public String UOM = "";
    public long STK_QTY = 0;
    public long QTY = 0;
    public String DEVICE_ID = "";

    @SuppressLint("DefaultLocale")
    @NonNull
    @Override
    public String toString() {
        return String.format("%s, %s, %s, %s, %s, %d, %d, %s",
                BIN_NO, BARCODE, PART_NO, PART_NAME, UOM, STK_QTY, QTY, DEVICE_ID);
    }
}