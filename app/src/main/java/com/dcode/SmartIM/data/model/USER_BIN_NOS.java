package com.dcode.SmartIM.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "USER_BIN_NOS")
public class USER_BIN_NOS {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "ID")
    public int ID;

    @ColumnInfo(name = "BIN_NO")
    public String BIN_NO = "";

    @ColumnInfo(name = "EXP_QTY")
    public int EXP_QTY = 0;

    @ColumnInfo(name = "CR_BY")
    public int CR_BY = 0;
}
