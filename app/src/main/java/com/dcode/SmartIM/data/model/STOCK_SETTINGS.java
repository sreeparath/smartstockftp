package com.dcode.SmartIM.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@Entity(tableName = "STOCK_SETTINGS")
public class STOCK_SETTINGS implements Serializable {


    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "ID")
    public int ID;

    @ColumnInfo(name = "ERP_TYPE")
    public int ERP_TYPE;

    @ColumnInfo(name = "STK_CODE")
    public String STK_CODE;

    @ColumnInfo(name = "DESCR")
    public String DESCR;

    @ColumnInfo(name = "SCH_START_DATE")
    public String SCH_START_DATE;

    @ColumnInfo(name = "SCH_END_DATE")
    public String SCH_END_DATE;

    @ColumnInfo(name = "ALLOW_NEW_BCODE")
    public int ALLOW_NEW_BCODE;

    @ColumnInfo(name = "REM_MAND_FOR_NEWBCODE")
    public int REM_MAND_FOR_NEWBCODE;

    @ColumnInfo(name = "LOCK_QTY")
    public int LOCK_QTY;

    @ColumnInfo(name = "ORCL_STORE")
    public String ORCL_STORE;

    @ColumnInfo(name = "ORCL_STK_ID")
    public String ORCL_STK_ID;

    @ColumnInfo(name = "BIN_FROM_LIST")
    public int BIN_FROM_LIST;
}