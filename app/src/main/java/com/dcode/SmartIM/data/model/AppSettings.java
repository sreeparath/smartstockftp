package com.dcode.SmartIM.data.model;

public class AppSettings {

    private String pdtId;

    public AppSettings() {
    }

    public AppSettings(String pdtId) {
        this.pdtId = pdtId;
    }

    public String getPdtId() {
        return pdtId;
    }

//    public void setPdtId(String pdtId) {
//        this.pdtId = pdtId;
//    }

}
