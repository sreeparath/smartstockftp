package com.dcode.SmartIM.data;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.dcode.SmartIM.data.model.BIN_ITEMS;
import com.dcode.SmartIM.data.model.ITEMMAST;
import com.dcode.SmartIM.data.model.TRANS_HDR;
import com.dcode.SmartIM.data.model.USER_BIN_NOS;

@Database(entities = {
        TRANS_HDR.class,
        BIN_ITEMS.class,
        ITEMMAST.class,
        USER_BIN_NOS.class,
}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase mInstance;

    static synchronized AppDatabase getInstance(Context context) {
        if (mInstance == null) {

            mInstance = Room.databaseBuilder(context, AppDatabase.class, "Data.db")
                    .allowMainThreadQueries()
                    .addCallback(new RoomDatabase.Callback() {
                        @Override
                        public void onCreate(@NonNull SupportSQLiteDatabase db) {
                            super.onCreate(db);
                        }

                        @Override
                        public void onOpen(@NonNull SupportSQLiteDatabase db) {
//                            attachDatabase(AppVariables.getMastersDb());
                            super.onOpen(db);
                        }
                    })
                    .build();
        }
        return mInstance;
    }

//    private static void attachDatabase(final String mastersDb) {
//        String sql = String.format("ATTACH DATABASE '%s' AS \"%s\";", mastersDb, "Items");
//        mInstance.mDatabase.execSQL(sql);
//    }

    public abstract GenericDao genericDao();
}
