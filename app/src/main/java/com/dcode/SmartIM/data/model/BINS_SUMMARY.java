package com.dcode.SmartIM.data.model;

import java.io.Serializable;

public class BINS_SUMMARY implements Serializable {

    public int PK_ID;
    public int MODULE_ID;
    public int TRANS_ID;
    public String BIN_NO;
    public String BIN_MODE;
    public String STORE_CODE;
    public String DEST_LOC;
    public String USER_NAME;
    public int ITEM_COUNT;
    public int IS_CHECKED;
}
