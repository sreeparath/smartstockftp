package com.dcode.SmartIM;

import android.app.Application;
import android.content.Context;

import com.dcode.SmartIM.common.AppPreferences;
import com.dcode.SmartIM.common.AppVariables;
import com.dcode.SmartIM.data.DatabaseClient;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;

public class App extends Application {
    public static final String TAG = "ERR_TAG";
    public static String DeviceID = "12345";
    public static String PDT_ID = "PDT-DEFAULT";
    private static Context context;
    //public static STOCK_SETTINGS stockSettings;

    public static DatabaseClient getDatabaseClient() {
        return DatabaseClient.getInstance(context);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("font/dubai_regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

        DeviceID = AppPreferences.getValue(context, AppPreferences.DEV_UNIQUE_ID, true);
        /* to init static members */
        new AppVariables(context);
    }
}
