package com.dcode.SmartIM.common;

import android.content.Context;
import android.util.Log;

import com.dcode.SmartIM.App;
import com.dcode.SmartIM.R;
import com.dcode.SmartIM.data.model.STOCK_SETTINGS;
import com.dcode.SmartIM.data.model.USERS;
import com.dcode.SmartIM.data.model.USER_BIN_NOS;
import com.dcode.SmartIM.data.model.USER_SETTINGS;

public class AppVariables {
    public static USERS CurrentUser = null;
    //    public static STORES CurrentStore = null;
    public static STOCK_SETTINGS CurrentStockSettings = null;
    public static USER_BIN_NOS CurrentBinNos = null;
    public static USER_SETTINGS CurrentUserSettings = null;
    private static String MASTERS_PATH;
    private static String IMAGES_PATH;
    private static String OUT_FILES_PATH;

    public AppVariables(Context context) {
        try {
            Utils.GetExternalSDCardPath(context);

            String sMastersData = Utils.getValue(context, AppConstants.MASTERS_PATH, false);
            if (sMastersData.isEmpty()) {
                sMastersData = Utils.getExternalSdPath().concat(context.getString(R.string.def_masters_path));
//            } else {
//                sMastersData = Utils.getExternalSdPath().concat(sMastersData);
            }
            MASTERS_PATH = sMastersData;

            String sImagesData = Utils.getValue(context, AppConstants.IMAGES_PATH, false);
            if (sImagesData.isEmpty()) {
                sImagesData = Utils.getExternalSdPath().concat(context.getString(R.string.def_images_path));
//            } else {
//                sImagesData = Utils.getExternalSdPath().concat(sImagesData);
            }
            IMAGES_PATH = sImagesData;

            String sOutFilesPath = context.getString(R.string.def_out_path); // Utils.getValue(context, AppConstants.OUT_PATH, false);
//            if (sOutFilesPath.isEmpty()) {
//                sOutFilesPath = Utils.getExternalSdPath().concat(context.getString(R.string.def_out_path));
//            } else {
//                sOutFilesPath = Utils.getExternalSdPath().concat(sOutFilesPath);
//            }
            OUT_FILES_PATH = sOutFilesPath;

        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }

    public static String getMastersPath() {
        return MASTERS_PATH;
    }

    public static void setMastersPath(String value) {
        MASTERS_PATH = value.endsWith("/") ? value : value + "/";
    }

    public static String getImagesPath() {
        return IMAGES_PATH;
    }

    public static void setImagesPath(String value) {
        IMAGES_PATH = value.endsWith("/") ? value : value + "/";
    }

    public static String getOutFilesPath() {
        return OUT_FILES_PATH;
    }

    public static void setOutFilesPath(String value) {
        OUT_FILES_PATH = value.endsWith("/") ? value : value + "/";
    }

    public static String getMastersDb() {
        return getMastersPath().concat("Masters.db");
    }

    public static String getAppSettingFile() {
        return getMastersPath().concat("AppSettings.txt");
    }


}
