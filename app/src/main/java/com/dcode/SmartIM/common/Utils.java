package com.dcode.SmartIM.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.sqlite.db.SimpleSQLiteQuery;

import com.dcode.SmartIM.App;
import com.dcode.SmartIM.data.model.GENERIC_COUNT;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class Utils {

    public static String DB_DATE_FORMAT = "yyyy-MMM-dd HH:mm:ss a";
    public static String FILE_NAME_FORMAT = "yyyyMMddHHmmss";  //yyyyMMddHHmm
    public static String SHORT_DATE_FORMAT = "ddMMyyyy";
    private static String EXTERNAL_SD_PATH = "/storage/emulated/0/";

    public static String getExternalSdPath() {
        return EXTERNAL_SD_PATH;
    }

    public synchronized static String getValue(Context context, String PREF_KEY, Boolean createRandom) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        String uniqueID = sharedPrefs.getString(PREF_KEY, null);
        if ((uniqueID == null || uniqueID.length() == 0) && createRandom) {
            uniqueID = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString(PREF_KEY, uniqueID);
            editor.apply();
        }
        if (uniqueID == null) {
            uniqueID = "";
        }
        return uniqueID;
    }

    public synchronized static boolean SetValue(Context context, String PREF_KEY, String value) {
        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(PREF_KEY, value);
        editor.apply();
        return true;
    }

    @SuppressLint("DefaultLocale")
    public static CustomResult CheckSTCountForUser(int ModuleID, String destLoc, String UserName) {

        CustomResult objResult = new CustomResult();
        objResult.errCode = "S";
        objResult.errMessage = "";
        objResult.isSuccess = true;
        String formatted;

        String strTemplateMode = "SELECT COUNT(1) AS TOTAL_COUNT FROM TRANS_HDR WHERE MODULE_ID = %d AND DEST_LOC = '%s' AND USER_NAME = '%s'";
        formatted = String.format(strTemplateMode, ModuleID, destLoc, UserName);

        SimpleSQLiteQuery squery = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(squery);
        int cnt = objCount.TOTAL_COUNT;
        if (cnt > 0) {
            objResult.errCode = "S";
            objResult.errMessage = "Some Transaction Present for user " + UserName;
            objResult.isSuccess = true;
        } else {
            objResult.errCode = "E";
            objResult.errMessage = "No Transaction Present for user " + UserName;
            objResult.isSuccess = false;
        }
        return objResult;
    }

    @SuppressLint("DefaultLocale")
    public static CustomResult CheckBinsForUser(int ModuleID, String BinNo, int binMode, String destLoc, String UserName) {

        CustomResult objResult = new CustomResult();
        objResult.errCode = "S";
        objResult.errMessage = "";
        objResult.isSuccess = true;
        String formatted;

        String strTemplateMode1 = "SELECT COUNT(1) AS TOTAL_COUNT FROM TRANS_HDR WHERE MODULE_ID = %d AND BIN_NO = '%s' AND BIN_MODE = %d AND USER_NAME = '%s'";
        String strTemplateMode2 = "SELECT COUNT(1) AS TOTAL_COUNT FROM TRANS_HDR WHERE MODULE_ID = %d AND BIN_NO = '%s' AND BIN_MODE = %d AND DEST_LOC = '%s' AND USER_NAME = '%s'";
        if (binMode == 1) {
            formatted = String.format(strTemplateMode1, ModuleID, BinNo, binMode, UserName);
        } else if (binMode == 2) {
            formatted = String.format(strTemplateMode2, ModuleID, BinNo, binMode, destLoc, UserName);
        } else {
            return null;
        }

        SimpleSQLiteQuery squery = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(squery);
        int cnt = objCount.TOTAL_COUNT;
        if (cnt > 0) {
            objResult.errCode = "S";
            objResult.errMessage = "Some Transaction Present in " + BinNo;
            objResult.isSuccess = true;
        } else {
            objResult.errCode = "E";
            objResult.errMessage = "No Transaction Present in " + BinNo;
            objResult.isSuccess = false;
        }
        return objResult;
    }

    public static int GetItemsCount(int trans_id, String bin_no) {
        String sqlTemplate = "SELECT SUM(QTY) TOTAL_COUNT FROM BIN_ITEMS WHERE TRANS_ID = %d AND BIN_NO = '%s'";
        String formatted = String.format(Locale.getDefault(), sqlTemplate, trans_id, bin_no);
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(query);
        return objCount.TOTAL_COUNT;
    }

    @SuppressLint("DefaultLocale")
    public static int GetBinsCount(int trans_id) {
        String template = "SELECT COUNT(DISTINCT ITM.BIN_NO) TOTAL_COUNT FROM TRANS_HDR HDR INNER JOIN BIN_ITEMS ITM ON ITM.TRANS_ID = HDR.PK_ID WHERE HDR.PK_ID = %d";
        String formatted = String.format(template, trans_id);
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(query);
        return objCount.TOTAL_COUNT;
    }

    @SuppressLint("DefaultLocale")
    public static int GetTotalBinItemsCount(int trans_id) {
        String template = "SELECT SUM(ITM.QTY) TOTAL_COUNT FROM TRANS_HDR HDR INNER JOIN BIN_ITEMS ITM  ON ITM.TRANS_ID = HDR.PK_ID WHERE HDR.PK_ID = %d";
        String formatted = String.format(template, trans_id);
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(query);
        return objCount.TOTAL_COUNT;
    }

    @SuppressLint("DefaultLocale")
    public static int GetSTBinsCount() {
        String template = "SELECT COUNT(DISTINCT ITM.BIN_NO) TOTAL_COUNT FROM TRANS_HDR HDR INNER JOIN BIN_ITEMS ITM ON ITM.TRANS_ID = HDR.PK_ID WHERE HDR.USER_NAME = '%s'";
        String formatted = String.format(template, AppVariables.CurrentUser.LOGIN_NAME);
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(query);
        return objCount.TOTAL_COUNT;
    }

    @SuppressLint("DefaultLocale")
    public static int GetSTTotalBinItemsCount() {
        String template = "SELECT SUM(ITM.QTY) TOTAL_COUNT FROM TRANS_HDR HDR INNER JOIN BIN_ITEMS ITM  ON ITM.TRANS_ID = HDR.PK_ID WHERE HDR.USER_NAME = '%s'";
        String formatted = String.format(template, AppVariables.CurrentUser.LOGIN_NAME);
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(query);
        return objCount.TOTAL_COUNT;
    }

    @SuppressLint("DefaultLocale")
    public static int GetSTBinItemsCount(int trans_id, String bin_no) {
        String template = "SELECT SUM(ITM.QTY) TOTAL_COUNT FROM BIN_ITEMS ITM WHERE TRANS_ID = %d AND BIN_NO = '%s'";
        String formatted = String.format(template, trans_id, bin_no);
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(formatted);
        GENERIC_COUNT objCount = App.getDatabaseClient().getAppDatabase().genericDao().getCount(query);
        return objCount.TOTAL_COUNT;
    }

    public static String GetCurrentDateTime(final String outputFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(outputFormat, Locale.getDefault());
        Date dateObj = new Date();
        return simpleDateFormat.format(dateObj);
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromDisk(final String filePath,
                                                     int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }

    private static boolean hasExternalSDCard() {
        try {
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
                return true;
        } catch (Throwable ignored) {
        }
        return false;
    }

    public static void GetExternalSDCardPath(Context context) {
        if (hasExternalSDCard()) {
            try {
                File[] files = context.getExternalFilesDirs(null);
                if (files == null) return;

                String sdPath = "";

                // Honeywell has IPSM Card
                for (File file : files) {
                    String currPath = file.getAbsolutePath();
                    if (!currPath.contains("emulated") && !currPath.contains("IPSM")) {
                        sdPath = currPath.substring(0, currPath.indexOf("Android/"));
                        break;
                    }
                }

                if (sdPath.isEmpty()) {
                    //no external sd card, so set to Downloads folder
                    EXTERNAL_SD_PATH = AppConstants.NO_EXT_PATH;
                    return;
                }
                File sdcard = new File(sdPath);
                if (sdcard.exists()) {
                    String value = sdcard.getAbsolutePath();
                    EXTERNAL_SD_PATH = value.endsWith("/") ? value : value.concat("/");
                }
            } catch (Exception e) {
                Log.d("SDPath", e.toString());
            }
        }
    }

    public static int convertToInt(String value, int defValue) {
        //String userdata = /*value from gui*/
        int val;
        try {
            val = Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            // bad data - set to sentinel
            val = defValue;
        }
        return val;
    }

    @NonNull
    public static String GetGUID() {
        return UUID.randomUUID().toString();
    }
}
