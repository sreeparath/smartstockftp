package com.dcode.SmartIM.common;

public class AppConstants {
//    public static final String DEV_UNIQUE_ID = "DEV_UNIQUE_ID";
//    public static final String PRINTER_MAC_ID = "PRINTER_MAC_ID";
//    public static final String SERVICE_URL = "SERVICE_URL";
//    public static final String PRINTER_MODEL = "PRINTER_MODEL";
    public static final String SELECTED_ID = "SELECTED_ID";
    public static final String SELECTED_CODE = "SELECTED_CODE";
    public static final String SELECTED_NAME = "SELECTED_NAME";
//    public static final String PARENT_ID = "PARENT_ID";
//    public static final String FILTER_KEY_INT_ARRAY = "FILTER_KEY_INT_ARRAY";
    public static final String SELECTED_OBJECT = "SELECTED_OBJECT";

    public static final String IMAGES_PATH = "IMAGES_PATH";
    public static final String MASTERS_PATH = "MASTERS_PATH";
//    public static final String OUT_PATH = "OUT_PATH";
    public static final String NO_EXT_PATH = "/storage/emulated/0/Download/";

//    public static final int ST_COUNT_BINS_QTY = 100;
//    public static final int ST_CONDITIONAL = 101;
//    public static final int ST_BLIND = 102;
//    public static final int ITEMS_VERIFICATION = 103;
//    public static final int TR_IN = 104;
//    public static final int TR_OUT = 105;
//
//    public static final String DEV_SERIAL = "DEV_SERIAL";
//    public static final String DEV_LICENSE_KEY = "DEV_LICENSE_KEY";
}
