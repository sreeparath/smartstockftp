package com.dcode.SmartIM.common;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.UUID;

public class AppPreferences {

    public static final String DEV_UNIQUE_ID = "DEV_UNIQUE_ID";
//    public static final String PRINTER_MAC_ID = "PRINTER_MAC_ID";
//    public static final String SERVICE_URL = "SERVICE_URL";
//    public static final String PRINTER_MODEL = "PRINTER_MODEL";


    public synchronized static String getValue(Context context, String PREF_KEY, Boolean createrandom) {
        String uniqueID = null;

        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);
        uniqueID = sharedPrefs.getString(PREF_KEY, null);
        if ((uniqueID == null || uniqueID.length() == 0) && createrandom) {
            uniqueID = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = sharedPrefs.edit();
            editor.putString(PREF_KEY, uniqueID);
            editor.apply();
        }
        if (uniqueID == null) {
            uniqueID = "";
        }
        return uniqueID;
    }

    public synchronized static boolean SetValue(Context context, String PREF_KEY, String Val) {
        //String uniqueID = null;

        SharedPreferences sharedPrefs = context.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString(PREF_KEY, Val);
        editor.apply();
        return true;
    }
}
