package com.dcode.SmartIM.common;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class CryptoSecurity {


    private static final byte[] key_Array = new byte[]{0x32, 0x1C, 0x1E, 0x1C, 0x32, 0x1C, 0x1E, 0x1C, 0x32, 0x1C, 0x1E, 0x1C, 0x32, 0x1C, 0x1E, 0x1C, 0x32, 0x1C, 0x1E, 0x1C, 0x32, 0x1C, 0x1E, 0x1C, 0x32, 0x1C, 0x1E, 0x1C, 0x32, 0x1C, 0x1E, 0x1C};
    private static final byte[] iv = new byte[]{0x32, 0x1C, 0x1E, 0x1C, 0x32, 0x1C, 0x1E, 0x1C, 0x32, 0x1C, 0x1E, 0x1C, 0x32, 0x1C, 0x1E, 0x1C};


    public static String encrypt(String strToEncrypt) {
        try {
            //Cipher _Cipher = Cipher.getInstance("AES");
            //Cipher _Cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            Cipher _Cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

            // Initialization vector.
            // It could be any value or generated using a random number generator.
            //byte[] iv = { 1, 2, 3, 4, 5, 6, 6, 5, 4, 3, 2, 1, 7, 7, 7, 7 };
            IvParameterSpec ivspec = new IvParameterSpec(iv);

            Key SecretKey = new SecretKeySpec(key_Array, "AES");
            _Cipher.init(Cipher.ENCRYPT_MODE, SecretKey, ivspec);

//            return Base64.encodeBase64String(_Cipher.doFinal(strToEncrypt.getBytes()));
            return new String(Base64.encodeBase64(_Cipher.doFinal(strToEncrypt.getBytes())));
        } catch (Exception e) {
            System.out.println("[Exception]:" + e.getMessage());
        }
        return null;
    }


    public static String decrypt(String EncryptedMessage) {
        try {
            //Cipher _Cipher = Cipher.getInstance("AES");
            //Cipher _Cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            Cipher _Cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");

            // Initialization vector.
            // It could be any value or generated using a random number generator.
            //byte[] iv = { 1, 2, 3, 4, 5, 6, 6, 5, 4, 3, 2, 1, 7, 7, 7, 7 };
            IvParameterSpec ivspec = new IvParameterSpec(iv);

            Key SecretKey = new SecretKeySpec(key_Array, "AES");
            _Cipher.init(Cipher.DECRYPT_MODE, SecretKey, ivspec);

            byte[] bytes = new org.apache.commons.codec.binary.Base64().decode(StringUtils.getBytesUtf8(EncryptedMessage));
            return new String(_Cipher.doFinal(bytes));

            //byte[] DecodedMessage = Base64.decodeBase64(EncryptedMessage);
            //return new String(_Cipher.doFinal(DecodedMessage));

        } catch (Exception e) {
            System.out.println("[Exception]:" + e.getMessage());
            e.printStackTrace();
        }
        return "";
    }

/*    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();

        sb.append("xml file string ...");

        String outputOfEncrypt = encrypt(sb.toString());
        System.out.println("[CryptoSecurity.outputOfEncrypt]:"+outputOfEncrypt);

        String outputOfDecrypt = decrypt(outputOfEncrypt);
        //String outputOfDecrypt = decrypt(sb.toString());
        System.out.println("[CryptoSecurity.outputOfDecrypt]:"+outputOfDecrypt);
    }*/
}
