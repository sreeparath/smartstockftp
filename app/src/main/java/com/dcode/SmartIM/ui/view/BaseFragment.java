package com.dcode.SmartIM.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.dcode.SmartIM.ui.dialog.CustomProgressDialog;

public abstract class BaseFragment extends Fragment {
    protected int ModuleID;

    private CustomProgressDialog progressDialog;
    private AlertDialog alertDialog;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
    }

    public void onBackPressed() {
        requireActivity().onBackPressed();
    }

    protected void showToast(final String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    protected void showAlert(final String title, final String message) {
        if (alertDialog != null && alertDialog.isShowing())
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        alertDialog = builder
                .setCancelable(false)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", (dialog, which) -> {
                })
                .create();
        alertDialog.show();
        shortVibration(1000);
    }

    protected void showProgress(boolean isCancelable) {
        if (progressDialog != null && progressDialog.isShowing())
            return;

        progressDialog = new CustomProgressDialog(getContext(), isCancelable);
        progressDialog.show();
    }

    void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    protected void shortVibration(int duration) {
        Vibrator vibe = (Vibrator) this.getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        if (vibe != null && vibe.hasVibrator()) vibe.vibrate(duration);
    }
}
