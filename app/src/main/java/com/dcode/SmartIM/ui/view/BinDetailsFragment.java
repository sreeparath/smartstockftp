package com.dcode.SmartIM.ui.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.SmartIM.App;
import com.dcode.SmartIM.R;
import com.dcode.SmartIM.adapter.BinDetailsAdapter;
import com.dcode.SmartIM.adapter.BinStockAdapter;
import com.dcode.SmartIM.common.AppVariables;
import com.dcode.SmartIM.data.model.STOCK_SETTINGS;
import com.dcode.SmartIM.data.model.USERS;
import com.dcode.SmartIM.data.model.USER_BIN_NOS;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BinDetailsFragment extends BaseFragment {

    private TextInputEditText Bin;
    private TextInputEditText Quantity;

    private BinDetailsAdapter viewAdapter;
    private RecyclerView rvItems;

    private String bin;

    private List<String> listItems;
    private STOCK_SETTINGS stockSettings;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
//        if (this.getArguments() != null) {
//            ModuleID = this.getArguments().getInt("ModuleID", 0);
//        }
        View root = inflater.inflate(R.layout.fragment_bin, container, false);

        Bin = root.findViewById(R.id.edBin);
        Quantity = root.findViewById(R.id.edQuantity);

        MaterialButton btnSave = root.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(v -> OnSaveClick(Bin.getText().toString(), Quantity.getText().toString()));

        rvItems = root.findViewById(R.id.rvBinList);
        ((DefaultItemAnimator) Objects.requireNonNull(rvItems.getItemAnimator())).setSupportsChangeAnimations(false);
        rvItems.setLayoutManager(new LinearLayoutManager(getContext()));

        viewAdapter = new BinDetailsAdapter(new ArrayList<>());
        rvItems.setAdapter(viewAdapter);
//        rvItems.setOnClickListener(this);
        viewAdapter.getBinData();

        MaterialButton btnProceed = root.findViewById(R.id.btnProceed);
        btnProceed.setOnClickListener(v -> OnNextClick());
        stockSettings = AppVariables.CurrentStockSettings;


        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {

                shortVibration(1000);
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setCancelable(false);
                alert.setTitle("Delete Bin ?");
                alert.setPositiveButton("Ok", (dialog, which) -> {
                    showToast("Deleted");
                    int position = viewHolder.getAdapterPosition();
                    viewAdapter.removeItem(position);
                    viewAdapter.notifyDataSetChanged();

                });
                alert.setNegativeButton("Cancel", (dialog, which) -> {
                    rvItems.setAdapter(viewAdapter);
                });
                alert.show();

            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rvItems);


        return root;


    }

    private boolean Validate() {
        String errMessage;
        if (stockSettings == null || stockSettings.DESCR == null || stockSettings.DESCR.isEmpty()) {
            errMessage = String.format("%s required", "STOCK_SETTINGS");
            showToast(errMessage);


            return false;
        }

        return true;
    }

    private void OnNextClick() {
        if (Validate()) {
            ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_stock_take_ftp, null);
        }
    }

    private void OnSaveClick(String bin, String quantity) {
        USERS user = AppVariables.CurrentUser;
        this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getBinNos();
        if (bin.isEmpty()) {
            showToast("Bin No. cannot be empty");
        } else {

            if (listItems.contains(bin)) {
                showToast("Duplicate entry of bin");
            } else {
                if (quantity.isEmpty()) {
                    quantity = "0";
                }

                try {

                    USER_BIN_NOS binNo = new USER_BIN_NOS();
                    binNo.BIN_NO = bin;
                    binNo.EXP_QTY = Integer.parseInt(quantity);
                    binNo.CR_BY = 0;
                    App.getDatabaseClient().getAppDatabase().genericDao().insertBin_No(binNo);
                    AppVariables.CurrentBinNos = binNo;
                    viewAdapter.getBinData();
                    viewAdapter.notifyDataSetChanged();
                    showToast("Saved");


                } catch (NumberFormatException e) {
                    Log.d(App.TAG, e.toString());
                    showAlert("Error", e.getMessage());
                }

            }
        }
    }
}
