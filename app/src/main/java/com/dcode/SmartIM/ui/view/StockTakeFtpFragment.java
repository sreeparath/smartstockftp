package com.dcode.SmartIM.ui.view;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.SmartIM.App;
import com.dcode.SmartIM.R;
import com.dcode.SmartIM.adapter.ListViewAdapter;
import com.dcode.SmartIM.common.AppVariables;
import com.dcode.SmartIM.common.Utils;
import com.dcode.SmartIM.data.MastersDBManager;
import com.dcode.SmartIM.data.model.BIN_ITEMS;
import com.dcode.SmartIM.data.model.FTP_SETTINGS;
import com.dcode.SmartIM.data.model.ITEM_MASTER;
import com.dcode.SmartIM.data.model.STOCK_SETTINGS;
import com.dcode.SmartIM.data.model.USERS;
import com.dcode.SmartIM.data.model.USER_BIN_NOS;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class StockTakeFtpFragment
        extends BaseFragment
        implements View.OnClickListener {

    private final ColorDrawable background = new ColorDrawable(Color.YELLOW);
    private TextInputEditText edLocCode;
    private TextInputEditText edBarCode;
    private TextInputEditText edQuantity;
    private CheckBox chkLockQuantity;
    private MaterialButton btnFTP;
    private RecyclerView rvItems;
    private ListViewAdapter viewAdapter;
    private MastersDBManager mastersDBManager;
    private Drawable icon;
    private String currLocCode;
    private String currBinNo;

    private STOCK_SETTINGS stockSettings;
    private USER_BIN_NOS userBinNos;
    private USERS user;

    private AutoCompleteTextView tvBin;
    private ArrayList options = new ArrayList();

    private boolean Lock_Qty = false;

    private FTP_SETTINGS ftpSettings = null;
    private List<String> listItems;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = !this.getArguments().containsKey("ModuleID") ? -1 : this.getArguments().getInt("ModuleID", -1);
        }

        View root = inflater.inflate(R.layout.fragment_stock_take_ftp, container, false);
        icon = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_delete_black, null);

        mastersDBManager = new MastersDBManager(AppVariables.getMastersDb());

        tvBin = root.findViewById(R.id.acBin);
        tvBin.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, options));
//        tvBin.dismissDropDown();
        currLocCode = "";
//        edLocCode = root.findViewById(R.id.edLocCode);
//        edLocCode.setOnKeyListener((v, keyCode, event) -> {
//            if (keyCode == KeyEvent.KEYCODE_ENTER) {
//                if (event.getAction() == KeyEvent.ACTION_DOWN) {
//                    onLocCodeKeyEvent();
//                } else {
//                    return true;
//                }
//                InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.hideSoftInputFromWindow(edBarCode.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
//                return true;
//            } else {
//                return false;
//            }
//        });

        edBarCode = root.findViewById(R.id.edBarCode);
        edBarCode.setOnKeyListener((v, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    Log.d("ERR", "DOWN");
                    onBarcodeKeyEvent();
//                } else {
//                    return true;
                }
                Log.d("ERR", "ENTER");
                InputMethodManager imm = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edBarCode.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                return true;
            } else {
                Log.d("ERR", "else");
                return false;
            }
        });

        chkLockQuantity = root.findViewById(R.id.chkLockQuantity);


        chkLockQuantity.setOnCheckedChangeListener((buttonView, isChecked) -> {

//            if (user.ALLOW_QTY_OVERRIDE == 1) {
//                if (stockSettings.LOCK_QTY == 1) {
//                    chkLockQuantity.setEnabled(true);
//                    edQuantity.setEnabled(!isChecked);
//                    chkLockQuantity.setChecked(isChecked);
//                    edQuantity.setText("1");
//                }
//            } else {
//                chkLockQuantity.setChecked(true);
//                chkLockQuantity.setEnabled(false);
//            }
            if (stockSettings.LOCK_QTY == 1) {
                if (user.ALLOW_QTY_OVERRIDE == 0) {
                    chkLockQuantity.setChecked(true);
                    chkLockQuantity.setEnabled(false);
                    edQuantity.setEnabled(false);
                    edQuantity.setText("1");
                }else{
                    chkLockQuantity.setChecked(true);
                    chkLockQuantity.setEnabled(true);
                    chkLockQuantity.setChecked(isChecked);
                    edQuantity.setEnabled(!chkLockQuantity.isChecked());
                    edQuantity.setText("1");
                }
            }
            else{
                chkLockQuantity.setChecked(true);
                chkLockQuantity.setEnabled(true);
                chkLockQuantity.setChecked(isChecked);
                edQuantity.setEnabled(!chkLockQuantity.isChecked());
                edQuantity.setText("1");
            }
        });

        edQuantity = root.findViewById(R.id.edQuantity);
//        edQuantity.setText("1");
        edQuantity.setEnabled(!chkLockQuantity.isChecked());
        edQuantity.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    keyCode == KeyEvent.KEYCODE_ENTER) {
                edBarCode.requestFocus();
                return true;
            }
            return false;
        });

        rvItems = root.findViewById(R.id.rvList);
        ((DefaultItemAnimator) Objects.requireNonNull(rvItems.getItemAnimator())).setSupportsChangeAnimations(false);
        rvItems.setLayoutManager(new LinearLayoutManager(getContext()));

        viewAdapter = new ListViewAdapter(new ArrayList<>(), this);
        rvItems.setAdapter(viewAdapter);
        rvItems.setOnClickListener(this);

        MaterialButton btnGenerate = root.findViewById(R.id.btnGenerate);
        btnGenerate.setOnClickListener(v -> onGenerateClick(false));

        btnFTP = root.findViewById(R.id.btnFTP);
        btnFTP.setOnClickListener(v -> onGenerateClick(true));

        MaterialButton btnBinStock = root.findViewById(R.id.btnBinStock);
        btnBinStock.setOnClickListener(v -> OnBinStockClick());


//        TextInputEditText edDesc = root.findViewById(R.id.edDesc);
//        TextInputEditText edRemark = root.findViewById(R.id.edRemark);
//        edDesc.setEnabled(false);
//        edRemark.setEnabled(false);

        return root;
    }

    public void OnBinStockClick() {
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.binStockFragment, null);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mastersDBManager = new MastersDBManager(AppVariables.getMastersDb());
        mastersDBManager.open();

        stockSettings = AppVariables.CurrentStockSettings;
        userBinNos = AppVariables.CurrentBinNos;
        user = AppVariables.CurrentUser;

        if (stockSettings.LOCK_QTY == 1) {
            Lock_Qty = true;
        }

        this.listItems = App.getDatabaseClient().getAppDatabase().genericDao().getBinNos();

        if (stockSettings.BIN_FROM_LIST == 1) {
            options.clear();


            for (int i = 0; i < listItems.size(); i++)
                options.add(listItems.get(i));
            tvBin.setEnabled(false);

        } else {
            tvBin.dismissDropDown();
        }

        boolean isEnableFTP = getFTPSettingData();
        RefreshUI();

        btnFTP.setEnabled(isEnableFTP);

        ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder viewHolder1) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                        if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
                            int position = viewHolder.getBindingAdapterPosition(); // .getAdapterPosition();
                            BIN_ITEMS binItems = viewAdapter.getItemByPosition(position);
                            if (binItems != null) {
                                final AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                                alert.setCancelable(false);
                                alert.setTitle("Delete this item?");
                                alert.setPositiveButton("OK", (dialog, whichButton) -> {
                                    App.getDatabaseClient().getAppDatabase().genericDao().deleteBinItemByID(binItems.PK_ID);
                                    viewAdapter.removeItem(position);
                                    RefreshUI();
                                });
                                alert.setNegativeButton("Cancel", (dialog, whichButton) -> {

                                    rvItems.setAdapter(viewAdapter);
                                    RefreshUI();
                                });
                                alert.show();
                                shortVibration(1000);
                            }
                        }
                    }

                    @Override
                    public void onChildDraw(@NonNull Canvas c,
                                            @NonNull RecyclerView recyclerView,
                                            @NonNull RecyclerView.ViewHolder viewHolder,
                                            float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                        View itemView = viewHolder.itemView;
                        int backgroundCornerOffset = 20;

                        int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
                        int iconBottom = iconTop + icon.getIntrinsicHeight();

                        if (dX > 0) {
                            int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                            int iconRight = itemView.getLeft() + iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getLeft(), itemView.getTop(),
                                    itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
                        } else if (dX < 0) {
                            int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                            int iconRight = itemView.getRight() - iconMargin;
                            icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                            background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                                    itemView.getTop(), itemView.getRight(), itemView.getBottom());
                        } else {
                            background.setBounds(0, 0, 0, 0);
                        }
                        background.draw(c);
                        icon.draw(c);
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(rvItems);
    }

    @Override
    public void onClick(View view) {
        /* nothing to do. handled in onViewCreated::ItemTouchHelper*/
    }

    private boolean getFTPSettingData() {
        try {
            showProgress(true);
            if (mastersDBManager != null) {
                ftpSettings = mastersDBManager.getFtpSettings();
                btnFTP.setEnabled(true);
            }

            if (ftpSettings == null) {
                showAlert("Error", "FTP settings not found in Master DB!");
                return false;
            }

            if (ftpSettings.HOST.isEmpty()) {
                showAlert("Error", "FTP Host not found in Master DB!");
                return false;
            }

            if (ftpSettings.PORT <= 0) {
                showAlert("Error", "FTP port not found in Master DB!");
                return false;
            }

            if (ftpSettings.TARGET_DIR.trim().isEmpty()) {
                showAlert("Error", "FTP directory not found in Master DB!");
                return false;
            }

            if (!ftpSettings.TARGET_DIR.startsWith("/")) {
                ftpSettings.TARGET_DIR = "/" + ftpSettings.TARGET_DIR.trim();
            }

            if (!ftpSettings.TARGET_DIR.endsWith("/")) {
                ftpSettings.TARGET_DIR = ftpSettings.TARGET_DIR.trim() + "/";
            }
            return true;
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showToast("Error accessing ftpSettings");
            return false;
        } finally {
            dismissProgress();
        }
    }

    private void onGenerateClick(boolean ftpFlag) {
        try {
            File filePath = new File(AppVariables.getOutFilesPath());
            if (!filePath.exists()) {
                boolean mkdir = filePath.mkdir();

                if (!mkdir) {
                    showAlert("Error", "Failed: create output folder");
                    return;
                }
            }
            WriteToFile(ftpFlag);
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage());
        }
    }

    private void WriteToFile(boolean flag) {
        try {

            if (viewAdapter.getItemCount() == 0 && !flag) {
                showAlert("Info", "Nothing to write.");
                return;
            }

            try {
                File filePath = new File(AppVariables.getOutFilesPath());
                String fileTimeStamp = Utils.GetCurrentDateTime(Utils.FILE_NAME_FORMAT);
                String fileNamePart = String.format(Locale.getDefault(), "%d_%d_%s.csv",
                        AppVariables.CurrentStockSettings.ID, AppVariables.CurrentUser.ID, fileTimeStamp);
                String itemsFileName = String.format(Locale.getDefault(), "%s/%s",
                        filePath, fileNamePart);
                WriteFileToDisk(itemsFileName, flag, fileNamePart);
            } catch (Exception e) {
                Log.d(App.TAG, e.getMessage());
            }

        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage());
            e.printStackTrace();
        }
    }

    private void WriteFileToDisk(String itemsFileName, boolean ftpFlag, String fileNamePart) {
        try {
            List<BIN_ITEMS> itemsList = App.getDatabaseClient().getAppDatabase().genericDao().getAllBIN_ITEMS();
            if (itemsList.size() > 0) {
                showProgress(false);
                Writer itemsOutput = new BufferedWriter(new FileWriter(itemsFileName, false));

                //id,_user_id,device_id,device_guid
                String fileIdentifier = String.format(Locale.getDefault(), "%d,%s,%s,%s\r\n",
                        AppVariables.CurrentStockSettings.ID,
                        AppVariables.CurrentUser.ID,
                        App.PDT_ID,
                        Utils.GetGUID());
                itemsOutput.append(fileIdentifier);

//                itemsOutput.append("SL NO, BARCODE, DESC, DEPT_CODE,STYLE CODE, SCANNED_QTY, DEVICE_ID\n");
                for (BIN_ITEMS item : itemsList) {
                    itemsOutput.append(item.toString()).append("\r\n");
                }
                itemsOutput.close();

                App.getDatabaseClient().getAppDatabase().genericDao().deleteAllBIN_ITEMS();
                viewAdapter.clearList();
                ClearUI();
            }

            if (ftpFlag) {
                if (ftpSettings != null) {
                    try {
                        uploadToFTP();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        showAlert("FTP", e.getMessage());
                    }
                } else {
                    showAlert("Error", "FTP Connection Error!");
                    return;
                }

            } else {
                showAlert("Info", "Files generated");
            }
        } catch (IOException e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage());
            return;
        } finally {
            dismissProgress();
        }
        MediaScannerConnection.scanFile(requireActivity(),
                new String[]{itemsFileName}, null, null);
    }

/*
    private void uploadToFTP(String OUT_FILENAME, String fileNamePart) throws InterruptedException {
        showProgress(false);
        final boolean[] status = {false};
        Thread ftpThread = new Thread(() -> {
//            String Target_DIR = String.format(Locale.getDefault(), "/S%d/", AppVariables.CurrentstockSettings.ID);
            String Target_DIR = ftpSettings.TARGET_DIR;
            Log.d("TEMP_FILENAME##", OUT_FILENAME);
            FtpService ftpService = new FtpService();
            try {
                status[0] = ftpService.UploadFile(ftpSettings.HOST,
                        ftpSettings.PORT,
                        ftpSettings.USER_NAME,
                        ftpSettings.PASSWORD,
                        Target_DIR,
                        fileNamePart,
                        OUT_FILENAME);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        ftpThread.start();
        ftpThread.join();

        String message = String.format("Upload %s %s", fileNamePart, status[0] ? "Success" : "Failed");
        showAlert("FTP", message);
    }
*/

    private void ClearUI() {
        //viewAdapter.refreshData();
        edBarCode.setText("");
        edBarCode.requestFocus();
    }

    private void RefreshUI() {
        try {

            viewAdapter.refreshData();

            if (chkLockQuantity.isChecked()) {
                edQuantity.setText("1");
                edQuantity.setEnabled(!chkLockQuantity.isChecked());
            }

            edBarCode.setText("");
            edBarCode.requestFocus();
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());

        }
    }

//    private void onLocCodeKeyEvent() {
//        try {
//            currLocCode = Objects.requireNonNull(edLocCode.getText()).toString().trim().replace("\r", "").replace("\n", "");
//        } catch (Exception ex) {
//            Log.d(App.TAG, ex.toString());
//            showAlert("Error", ex.getMessage());
//        }
//
//        tvBin.requestFocus();
////        edBarCode.requestFocus();
//    }

    private void onBarcodeKeyEvent() {
        try {
            String barcode = Objects.requireNonNull(edBarCode.getText()).toString().trim().replace("\r", "").replace("\n", "");

            if (barcode.isEmpty()) {
                return;
            }

//            currLocCode = edLocCode.getText().toString();
//            if (currLocCode.isEmpty()) {
//                showToast("Confirm Loc code");
//                return;
//            }

            currBinNo = tvBin.getText().toString();
            if (currBinNo.isEmpty()) {
                showToast("Confirm Bin No");
                return;
            }


            String sQty = Objects.requireNonNull(edQuantity.getText()).toString().trim();
            int iQty = Utils.convertToInt(sQty, -1);
            if (iQty <= 0) {
                showAlert("Stop", "Invalid Quantity");
                return;
            }

            ITEM_MASTER items = mastersDBManager.getFtpStockMastByBarcode(barcode);
            if (items != null) {
//                items.BIN_NO=edLocCode.getText().toString();
                AddToBin(items, iQty, 0);
                RefreshUI();
            } else if (AppVariables.CurrentStockSettings.ALLOW_NEW_BCODE == 1) {
                AddNewItem(barcode, iQty);
            } else {
                showAlert("Stop", "Invalid Barcode");
            }
            edBarCode.setText("");
        } catch (Exception ex) {
            Log.d(App.TAG, ex.toString());
            showAlert("Error", ex.getMessage());
        }

        edBarCode.requestFocus();
    }

    private void AddToBin(ITEM_MASTER items, int quantity, int isNewItem) {
        try {

            BIN_ITEMS bin_items = new BIN_ITEMS();
            bin_items.BIN_NO = currBinNo;
            bin_items.TRANS_GUID = Utils.GetGUID();
            bin_items.SCAN_TIME = Utils.GetCurrentDateTime(Utils.DB_DATE_FORMAT);
            bin_items.DEVICE_ID = App.PDT_ID;
            bin_items.LOGON_NAME = String.valueOf(AppVariables.CurrentUser.ID);
            bin_items.LOC_CODE = currBinNo;
            bin_items.QTY = quantity;
            bin_items.BARCODE = items.BARCODE;
            bin_items.PART_NO = items.ITEM_CODE;
            bin_items.PART_NAME = items.ITEM_DESC;
            bin_items.STK_QTY = quantity;
            bin_items.UOM = items.UOM;
            bin_items.BIN_MODE = items.DEPT_CODE;
            bin_items.IS_NEW = isNewItem;
            Log.d("bin_items.BARCODE##", bin_items.BARCODE);
            App.getDatabaseClient().getAppDatabase().genericDao().insertBin_Items(bin_items);

        } catch (NumberFormatException e) {
            Log.d(App.TAG, e.toString());
            showAlert("Error", e.getMessage());
        }
        edBarCode.requestFocus();
    }

    private void AddNewItem(String barCode, int iQty) {
        Context context = requireActivity();
        String message = String.format(Locale.getDefault(), "Enter Item Name\r\nBarcode: %s\r\nQuantity:%d", barCode, iQty);
        final TextInputEditText itemNameEditText = new TextInputEditText(context);
        new AlertDialog.Builder(context)
                .setCancelable(false)
                .setTitle("Add new Item")
                .setMessage(message)
                .setView(itemNameEditText)
                .setPositiveButton("OK", (dialog, which) -> {
                    try {
                        ITEM_MASTER items = new ITEM_MASTER();

                        items.ITEM_DESC = itemNameEditText.getText().toString();
                        items.BARCODE = barCode;
                        items.STYLE_CODE = "";
                        items.UOM = "";
                        items.DEPT_CODE = "";
                        AddToBin(items, iQty, 1);
                        RefreshUI();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                })
                .setNegativeButton("Cancel", (dialog, which) -> {
                })
                .show();
        shortVibration(1000);
    }

    private void uploadToFTP() throws InterruptedException {
        showProgress(false);

        final boolean[] uploadStatus = new boolean[]{true};
        try {
            Thread ftpThread = new Thread(() -> {
                boolean status;
                String Target_DIR = ftpSettings.TARGET_DIR;
                FtpService ftpService = new FtpService();
                try {
                    File directory = new File(AppVariables.getOutFilesPath());
                    File[] files = directory.listFiles();
                    if (files == null)
                        return;

                    for (File file : files) {
                        if (file.getName().endsWith(".csv")) {
                            status = ftpService.UploadFile(ftpSettings.HOST,
                                    ftpSettings.PORT,
                                    ftpSettings.USER_NAME,
                                    ftpSettings.PASSWORD,
                                    Target_DIR,
                                    file.getName(),
                                    file.getAbsolutePath());

                            if (status) {
                                File fileBak = new File(file.getAbsoluteFile() + ".bak");
                                file.renameTo(fileBak);
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    uploadStatus[0] = false;
                }
            });
            ftpThread.start();
            ftpThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw e;
        } finally {
            String message = String.format("Upload %s", uploadStatus[0] ? "Success" : "Failed");
            showAlert("FTP", message);
            dismissProgress();
        }
    }
}
