package com.dcode.SmartIM.ui.view;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.SmartIM.App;
import com.dcode.SmartIM.R;
import com.dcode.SmartIM.adapter.BinStockAdapter;
import com.dcode.SmartIM.adapter.ListViewAdapter;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.Objects;

public class BinStockFragment extends BaseFragment {
    private BinStockAdapter viewAdapter;
    private RecyclerView rvItems;
    private TextView totalQty;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_bin_stock, container, false);


        rvItems =root.findViewById(R.id.rvList);
        ((DefaultItemAnimator) Objects.requireNonNull(rvItems.getItemAnimator())).setSupportsChangeAnimations(false);
        rvItems.setLayoutManager(new LinearLayoutManager(getContext()));

        viewAdapter = new BinStockAdapter(new ArrayList<>());
        rvItems.setAdapter(viewAdapter);
        viewAdapter.notifyDataSetChanged();
//        rvItems.setOnClickListener(this);
        viewAdapter.getBinStockData();

        totalQty=root.findViewById(R.id.tvQty);
       String qty= App.getDatabaseClient().getAppDatabase().genericDao().getQty();
       if(qty==null) {
           totalQty.setText("0");
       }else {
           totalQty.setText(qty);

       }

       MaterialButton RefreshButton=root.findViewById(R.id.btnRefresh);
        RefreshButton.setOnClickListener(v -> OnRefrshClick());

        MaterialButton backButton=root.findViewById(R.id.btnBack);
        backButton.setOnClickListener(v -> OnBackClick());

        if(viewAdapter.getBinStockData()==false){
            showToast("No Bin Available");
        }
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {

                shortVibration(1000);
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setCancelable(false);
                alert.setTitle("Delete Bin ?");
                alert.setPositiveButton("Ok", (dialog, which) -> {
                    showToast("Deleted");
                    int position = viewHolder.getAdapterPosition();
                    viewAdapter.removeItem(position);

                });
                alert.setNegativeButton("Cancel", (dialog, which) -> {
                    rvItems.setAdapter(viewAdapter);
                });
                alert.show();

            }
        };




        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rvItems);

        return root;
    }

    private void OnBackClick() {
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_stock_take_ftp, null);
    }

    private void OnRefrshClick() {
        ((MainActivity) requireActivity()).NavigateToFragment(R.id.binStockFragment, null);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);

        // Refresh tab data:

        if (getFragmentManager() != null) {

            getFragmentManager()
                    .beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit();
        }
    }


}
