package com.dcode.SmartIM.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.navigation.Navigation;

import com.dcode.SmartIM.R;

public class HomeFragment
        extends BaseFragment {

    View root;
    ConstraintLayout cl_stock_take_0;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);


        root = inflater.inflate(R.layout.fragment_home, container, false);

        cl_stock_take_0 = root.findViewById(R.id.cl_stock_take_0);
        TextView tv_bg_stock_take_0 = root.findViewById(R.id.tv_bg_stock_take_0);
        tv_bg_stock_take_0.setOnClickListener(v -> OnModuleButtonClick(R.id.action_nav_home_to_nav_stock_take_ftp));

        TextView tv_bg_logout = root.findViewById(R.id.tv_bg_logout);
        tv_bg_logout.setOnClickListener(v -> OnLogoutClick());

        return root;
    }

    private void OnModuleButtonClick(int action_id) {
        Navigation.findNavController(root).navigate(action_id);
    }

    private void OnLogoutClick() {
        ((MainActivity) requireActivity()).SignOut();
    }
}
