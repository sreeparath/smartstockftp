package com.dcode.SmartIM.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.ui.AppBarConfiguration;

import com.dcode.SmartIM.App;
import com.dcode.SmartIM.R;
import com.dcode.SmartIM.common.AppVariables;
import com.dcode.SmartIM.data.MastersDBManager;
import com.dcode.SmartIM.data.model.USERS;
import com.google.android.material.button.MaterialButton;

import java.io.File;

public class BinLoginFragment extends BaseFragment {

    private EditText edUserName;
    private EditText edPassword;
    private MastersDBManager mastersDBManager;
    private USERS curUser = null;
    private NavController mNavController = null;
    private AppBarConfiguration mAppBarConfiguration;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//                View.OnFocusChangeListener ofcListener = new BinLoginFragment.KeyboardHideListener();

        View root = inflater.inflate(R.layout.fragment_bin_login, container, false);

        edUserName = root.findViewById(R.id.edUserName);
//        edUserName.setOnFocusChangeListener(ofcListener);

        edPassword = root.findViewById(R.id.edPassword);
//        edPassword.setOnFocusChangeListener(ofcListener);


        MaterialButton btnLogin = root.findViewById(R.id.btSignIn);
        btnLogin.setOnClickListener(v -> {
            try {
                OnClick_Login();
            } catch (Exception e) {
                Log.d(App.TAG, e.toString());
            }
        });

        MaterialButton btnBack = root.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v ->{onBackPressed();});

                File masterDB = new File(AppVariables.getMastersDb());
        if (masterDB.exists()) {
            showToast("Master db present");
        }



        mastersDBManager = new MastersDBManager(AppVariables.getMastersDb());

        return root;
    }


    private void OnClick_Login() {

        try {
            if (!checkMasterDataBase(true))
                return;

            String uname = edUserName.getText().toString().trim();
            if (uname.isEmpty()) {
                edUserName.setError("User name required");
                showToast("User name required");
                return;
            }

            String uPass = edPassword.getText().toString().trim();
            if (uPass.isEmpty()) {
                edPassword.setError("Password required");
                showToast("Password required");
                return;
            }
            showProgress(false);
            if (ValidateUser(uname, uPass)) {
              if(curUser.ALLOW_BIN_CREATION==1 ){

                  ((MainActivity) requireActivity()).NavigateToFragment(R.id.locationFragment, null);

              }
              else{
                  showToast("");
              }
            } else {
                showToast("Invalid Credentials!");
            }

        } catch (Exception e) {
            e.printStackTrace();
            showAlert("Error", e.getMessage());
        }
    }
    private boolean ValidateUser(String uName, String uPass) {
        boolean status = false;


        try {
            showProgress(true);
            if (mastersDBManager != null) {
                mastersDBManager.open();
                curUser = mastersDBManager.getUser(uName, uPass);
                mastersDBManager.close();
            }
            if (curUser != null && curUser.LOGIN_NAME.length() > 0) {
                AppVariables.CurrentUser = curUser;
                status = true;
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showToast("Error accessing Users Master");
        } finally {
            dismissProgress();
        }

        return status;
    }
    private boolean checkMasterDataBase(boolean showAlert) {
        try {
            File masterDB = new File(AppVariables.getMastersDb());
            if (!masterDB.exists()) {
                if (showAlert) {
                    showAlert("Stop", "Master database not found.");
                }
                showToast("Db present");
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            showAlert("Error", e.getMessage());
        }
        return true;
    }


}
