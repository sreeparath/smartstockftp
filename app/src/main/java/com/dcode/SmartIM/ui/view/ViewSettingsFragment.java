package com.dcode.SmartIM.ui.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.dcode.SmartIM.R;
import com.dcode.SmartIM.common.AppVariables;
import com.dcode.SmartIM.data.model.STOCK_SETTINGS;
import com.dcode.SmartIM.data.model.USERS;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

public class ViewSettingsFragment extends BaseFragment {

    private TextInputEditText tvErrType;
    private TextInputEditText tvStkCode;
    private TextInputEditText tvDesc;
//    private TextInputEditText tvSchStDate;
//    private TextInputEditText tvSchEndDate;
//    private TextInputEditText tvAllowNew;
//    private TextInputEditText tvRemNewBarcode;
//    private TextInputEditText tvLockQty;
//    private TextInputEditText tvOrclStore;
//    private TextInputEditText tvOrclStkId;

    private STOCK_SETTINGS stockSettings;
    private USERS user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (this.getArguments() != null) {
            ModuleID = this.getArguments().getInt("ModuleID", 0);
        }

        View root = inflater.inflate(R.layout.fragment_settings_view, container, false);

        tvErrType = root.findViewById(R.id.tvErrType);
        tvStkCode = root.findViewById(R.id.tvStkCode);
        tvDesc = root.findViewById(R.id.tvDesc);

//        tvSchStDate = root.findViewById(R.id.tvSchStDate);
//        tvSchEndDate = root.findViewById(R.id.tvSchendDate);
//        tvAllowNew = root.findViewById(R.id.tvAllowNew);
//        tvRemNewBarcode = root.findViewById(R.id.tvRemNewBarcode);
//        tvLockQty = root.findViewById(R.id.tvLockQty);
//        tvOrclStore = root.findViewById(R.id.tvOrclStore);
//        tvOrclStkId = root.findViewById(R.id.tvOrclStkId);

        MaterialButton btnProceed = root.findViewById(R.id.btnProceed);
        btnProceed.setOnClickListener(v -> OnNextClick());
        MaterialButton btnBack = root.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> {
            ((MainActivity) requireActivity()).SignOut();
        });

        MaterialButton btnAddBin = root.findViewById(R.id.btnAddBin);
        btnAddBin.setOnClickListener(v -> OnAddBinClick());

        return root;
    }

    private void OnAddBinClick() {
        if(user.ALLOW_BIN_CREATION ==1&&stockSettings.BIN_FROM_LIST==1)
        {            ((MainActivity) requireActivity()).NavigateToFragment(R.id.locationFragment, null);       }

        else
        {
            if(stockSettings.BIN_FROM_LIST==1){
            showToast("NO PERMISSION");
            ((MainActivity) requireActivity()).NavigateToFragment(R.id.binLoginFragment,null);}
            else
                showToast("NO PERMISSION");
        }

    }

    private void finish() {
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        stockSettings = AppVariables.CurrentStockSettings;
        user = AppVariables.CurrentUser;
        RefreshUI();
    }

    private void OnNextClick() {
        if (Validate()) {
            ((MainActivity) requireActivity()).NavigateToFragment(R.id.nav_stock_take_ftp, null);
        }
    }

    private boolean Validate() {
        String errMessage;
        if (stockSettings == null || stockSettings.DESCR == null || stockSettings.DESCR.isEmpty()) {
            errMessage = String.format("%s required", "STOCK_SETTINGS");
            showToast(errMessage);
            tvDesc.setError(errMessage);

            return false;
        }

        return true;
    }

    private void RefreshUI() {
        try {
            tvErrType.setText(stockSettings.ERP_TYPE == 1 ? "Navision" : "Oracle");
            tvStkCode.setText(stockSettings.STK_CODE);
            tvDesc.setText(stockSettings.DESCR);
        } catch (Exception e) {
            e.printStackTrace();
            showToast(e.getMessage());
        }
    }
}
