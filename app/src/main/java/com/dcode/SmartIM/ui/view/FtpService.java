package com.dcode.SmartIM.ui.view;

import android.util.Log;
import android.widget.Toast;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FtpService {

    // Now, declare a public FTP client object.

    private static final String TAG = "FtpService";
    public FTPClient mFTPClient = null;
    private final boolean mResult = false;

    // Method to connect to FTP server:
    public boolean ftpConnect(String host, String username, String password,
                              int port) {
        try {
            mFTPClient = new FTPClient();
            // connecting to the host
            mFTPClient.connect(host, port);

            // now check the reply code, if positive mean connection success
            if (FTPReply.isPositiveCompletion(mFTPClient.getReplyCode())) {
                // login using username & password
                boolean status = mFTPClient.login(username, password);

                /*
                 * Set File Transfer Mode
                 *
                 * To avoid corruption issue you must specified a correct
                 * transfer mode, such as ASCII_FILE_TYPE, BINARY_FILE_TYPE,
                 * EBCDIC_FILE_TYPE .etc. Here, I use BINARY_FILE_TYPE for
                 * transferring text, image, and compressed files.
                 */
                mFTPClient.setFileType(FTP.BINARY_FILE_TYPE);
                mFTPClient.enterLocalPassiveMode();

                return status;
            }
        } catch (Exception e) {
            Log.d(TAG, "Error: could not connect to host " + host);
        }

        return false;
    }

    // Method to disconnect from FTP server:

    public boolean ftpDisconnect() {
        try {
            mFTPClient.logout();
            mFTPClient.disconnect();
            return true;
        } catch (Exception e) {
            Log.d(TAG, "Error occurred while disconnecting from ftp server.");
        }

        return false;
    }

    // Method to get current working directory:

    public String ftpGetCurrentWorkingDirectory() {
        try {
            String workingDir = mFTPClient.printWorkingDirectory();
            return workingDir;
        } catch (Exception e) {
            Log.d(TAG, "Error: could not get current working directory.");
        }

        return null;
    }

    // Method to change working directory:

    public boolean ftpChangeDirectory(String directory_path) {
        try {
            mFTPClient.changeWorkingDirectory(directory_path);
        } catch (Exception e) {
            Log.d(TAG, "Error: could not change directory to " + directory_path);
        }

        return false;
    }

    // Method to list all files in a directory:

    public String[] ftpPrintFilesList(String dir_path) {
        String[] fileList = null;
        try {
            FTPFile[] ftpFiles = mFTPClient.listFiles(dir_path);
            int length = ftpFiles.length;
            fileList = new String[length];
            for (int i = 0; i < length; i++) {
                String name = ftpFiles[i].getName();
                boolean isFile = ftpFiles[i].isFile();

                if (isFile) {
                    fileList[i] = "File :: " + name;
                    Log.i(TAG, "File : " + name);
                } else {
                    fileList[i] = "Directory :: " + name;
                    Log.i(TAG, "Directory : " + name);
                }
            }
            return fileList;
        } catch (Exception e) {
            e.printStackTrace();
            return fileList;
        }
    }

    // Method to create new directory:

    public boolean ftpMakeDirectory(String new_dir_path) {
        try {
            boolean status = mFTPClient.makeDirectory(new_dir_path);
            return status;
        } catch (Exception e) {
            Log.d(TAG, "Error: could not create new directory named "
                    + new_dir_path);
        }

        return false;
    }

    // Method to delete/remove a directory:

    public boolean ftpRemoveDirectory(String dir_path) {
        try {
            boolean status = mFTPClient.removeDirectory(dir_path);
            return status;
        } catch (Exception e) {
            Log.d(TAG, "Error: could not remove directory named " + dir_path);
        }

        return false;
    }

    // Method to delete a file:

    public boolean ftpRemoveFile(String filePath) {
        try {
            boolean status = mFTPClient.deleteFile(filePath);
            return status;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    // Method to rename a file:

    public boolean ftpRenameFile(String from, String to) {
        try {
            boolean status = mFTPClient.rename(from, to);
            return status;
        } catch (Exception e) {
            Log.d(TAG, "Could not rename file: " + from + " to: " + to);
        }

        return false;
    }

    // Method to download a file from FTP server:

    /**
     * mFTPClient: FTP client connection object (see FTP connection example)
     * srcFilePath: path to the source file in FTP server desFilePath: path to
     * the destination file to be saved in sdcard
     */
    public boolean ftpDownload(String srcFilePath, String desFilePath) {
        boolean status = false;
        try {
            FileOutputStream desFileStream = new FileOutputStream(desFilePath);
            status = mFTPClient.retrieveFile(srcFilePath, desFileStream);
            desFileStream.close();

            return status;
        } catch (Exception e) {
            Log.d(TAG, "download failed");
        }

        return status;
    }

    // Method to upload a file to FTP server:

    /**
     * mFTPClient: FTP client connection object (see FTP connection example)
     * srcFilePath: source file path in sdcard desFileName: file name to be
     * stored in FTP server desDirectory: directory path where the file should
     * be upload to
     */
    /*public boolean ftpUpload(String srcFilePath, String desFileName,
                             String desDirectory, Context context) {
        boolean status = false;
        try {
            FileInputStream srcFileStream = new FileInputStream(srcFilePath);

            mFTPClient.setFileType(FTP.BINARY_FILE_TYPE, FTP.BINARY_FILE_TYPE);

            mFTPClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);

            mFTPClient.enterLocalPassiveMode();
            status = mFTPClient.storeFile(desFileName, srcFileStream);

            srcFileStream.close();

            return status;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "upload failed: " + e);
            return false;
        }

        //return status;
    }*/

    /*public boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }*/

    /*public void connectToFTP(String hostAddr, int ports, String user, String pass) {

        //final boolean retfalg = false;

        final String host = hostAddr;//"192.168.0.102";
        final String username = user;//"krishnan";
        final String password = pass;//"123456";
        final int port = ports;//"123456";

//        final String host = "192.168.0.102";
//        final String username = "krishnan";
//        final String password = "123456";
//        final int port = 21;

//        if (host.length() < 1) {
//            Toast.makeText(MainActivity.this, "Please Enter Host Address!",
//                    Toast.LENGTH_LONG).show();
//        } else if (username.length() < 1) {
//            Toast.makeText(MainActivity.this, "Please Enter User Name!",
//                    Toast.LENGTH_LONG).show();
//        } else if (password.length() < 1) {
//            Toast.makeText(MainActivity.this, "Please Enter Password!",
//                    Toast.LENGTH_LONG).show();
//        } else {

        new Thread(new Runnable() {
            public void run() {
                boolean status = false;
                status = ftpConnect(host, username, password, port);
                if (status == true) {
                    Log.d(TAG, "Connection Success");
                } else {
                    Log.d(TAG, "Connection failed");
                }
            }
        }).start();
        //}
        //return false;
    }*/

/*
    public boolean connectToFTPandDownload(String hostAddr,int ports,String user,String pass,String srcFile, String srcFileFolder,String desFilePath) {

        //final boolean retfalg = false;

        final String host = hostAddr;//"192.168.0.102";
        final String username = user;//"krishnan";
        final String password = pass;//"123456";
        final int port = ports;//"123456";

//        final String host = "192.168.0.102";
//        final String username = "krishnan";
//        final String password = "123456";
//        final int port = 21;

//        if (host.length() < 1) {
//            Toast.makeText(MainActivity.this, "Please Enter Host Address!",
//                    Toast.LENGTH_LONG).show();
//        } else if (username.length() < 1) {
//            Toast.makeText(MainActivity.this, "Please Enter User Name!",
//                    Toast.LENGTH_LONG).show();
//        } else if (password.length() < 1) {
//            Toast.makeText(MainActivity.this, "Please Enter Password!",
//                    Toast.LENGTH_LONG).show();
//        } else {

        mResult = false;
        final Handler handler = new Handler(Looper.getMainLooper()) {

            @Override
            public void handleMessage(Message mesg) {
                throw new RuntimeException();
            }
        };


        new Thread(new Runnable() {
            public void run() {
                boolean status = false;
                status = ftpConnect(host, username, password, port);
                if (status == true) {
                    Log.d(TAG, "Connection Success");
                    status  = ftpDownload(srcFileFolder+"/"+srcFile,desFilePath);
                    if (status == true) {
                        unpackZip(AppVariables.getMastersPath(), "ItemMast.zip");
                        mResult = true;
                        handler.sendMessage(handler.obtainMessage());
                    }else{
                        mResult = true;
                        handler.sendMessage(handler.obtainMessage());
                        Log.d(TAG, "Download  failed");
                    }
                } else {
                    Log.d(TAG, "Connection failed");
                    mResult = true;
                    handler.sendMessage(handler.obtainMessage());
                }
            }
        }).start();
        //}
        return mResult;
    }
*/
    public boolean UnpackZip(String path, String zipname) {
        InputStream inputStream;
        ZipInputStream zipInputStream;
        try {
            inputStream = new FileInputStream(path + zipname);
            zipInputStream = new ZipInputStream(new BufferedInputStream(inputStream));
            ZipEntry zipEntry;

            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int count;

                FileOutputStream fout = new FileOutputStream(path + zipEntry.getName());

                // reading and writing
                while ((count = zipInputStream.read(buffer)) != -1) {
                    baos.write(buffer, 0, count);
                    byte[] bytes = baos.toByteArray();
                    fout.write(bytes);
                    baos.reset();
                }

                fout.close();
                zipInputStream.closeEntry();
            }

            zipInputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean UnpackMastersDbZip(String path, String zipname, String dbName) {
        InputStream inputStream;
        ZipInputStream zipInputStream;
        try {
            ZipEntry zipEntry;
            inputStream = new FileInputStream(path + zipname);
            zipInputStream = new ZipInputStream(new BufferedInputStream(inputStream));
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                String entryName = zipEntry.getName();
                if (entryName.endsWith(dbName)) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[1024];
                    int count;

                    FileOutputStream fout = new FileOutputStream(path + dbName);
                    // reading and writing
                    while ((count = zipInputStream.read(buffer)) != -1) {
                        baos.write(buffer, 0, count);
                        byte[] bytes = baos.toByteArray();
                        fout.write(bytes);
                        baos.reset();
                    }
                    fout.close();
                }
                zipInputStream.closeEntry();
            }
            zipInputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean DownloadFile(String server, int portNumber,
                                String user, String password, String filename, File localFile)
            throws IOException {
        FTPClient ftp = null;
        try {
            ftp = new FTPClient();
            ftp.connect(server, portNumber);
            Log.d(TAG, "Connected. Reply: " + ftp.getReplyString());
            ftp.login(user, password);
            Log.d(TAG, ftp.getReplyString());
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            Log.d(TAG, ftp.getReplyString());
            ftp.enterLocalPassiveMode();
            Log.d(TAG, ftp.getReplyString());


            OutputStream outputStream = null;
            boolean success = false;
            try {
                outputStream = new BufferedOutputStream(new FileOutputStream(
                        localFile));
                success = ftp.retrieveFile(filename, outputStream);
            } finally {
                if (outputStream != null) {
                    outputStream.close();
                }
            }

            return success;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (ftp != null) {
                ftp.logout();
                ftp.disconnect();
            }
        }
    }

    public boolean UploadFile(String server, int portNumber,
                              String user, String password, String hostDir, String filename, String localFile)
            throws IOException {
        FTPClient ftp = null;

        boolean success = false;

        try {
            ftp = new FTPClient();
            ftp.connect(server, portNumber);
            Log.d(TAG, "Connected. Reply: " + ftp.getReplyString());

            ftp.login(user, password);
            Log.d(TAG, "Logged in");
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            Log.d(TAG, "Uploading");
            ftp.enterLocalPassiveMode();

            try (InputStream input = new FileInputStream(new File(localFile))) {
                ftp.storeFile(hostDir + filename, input);
            } catch (Exception e) {
                e.printStackTrace();
            }

            success = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ftp != null) {
                ftp.logout();
                ftp.disconnect();
            }
        }
        return success;
    }
}