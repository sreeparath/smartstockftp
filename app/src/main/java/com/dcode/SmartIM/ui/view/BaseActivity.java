package com.dcode.SmartIM.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.dcode.SmartIM.common.AppConstants;
import com.dcode.SmartIM.ui.dialog.CustomProgressDialog;

public abstract class BaseActivity extends AppCompatActivity {

    protected int selectedID;
    protected String selectedCode;
    protected String selectedName;
    protected AlertDialog alertDialog;
    protected CustomProgressDialog progressDialog;

    protected abstract int getLayoutResourceId();

    protected abstract void onViewReady(Bundle savedInstanceState);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());
        onViewReady(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode != Activity.RESULT_OK) return;

        selectedID = intent.getIntExtra(AppConstants.SELECTED_ID, -1);
        selectedCode = intent.getStringExtra(AppConstants.SELECTED_CODE);
        selectedName = intent.getStringExtra(AppConstants.SELECTED_NAME);
    }

    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    protected void showToastShort(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected void showAlert(String title, String message, final boolean isCancellable) {
        if (alertDialog != null && alertDialog.isShowing())
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        alertDialog = builder.setTitle(title).setMessage(message).setPositiveButton("OK", (dialog, which) -> {
            if (isCancellable) {
                finish();
            }
        }).create();
        alertDialog.setCancelable(isCancellable);
        alertDialog.show();
        shortVibration();
        ringtone();
    }

    protected void showProgress(boolean isCancelable) {
        if (progressDialog != null && progressDialog.isShowing())
            return;

        progressDialog = new CustomProgressDialog(this, isCancelable);
        progressDialog.show();
    }

    protected void dismissProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    protected void shortVibration() {
        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (vibe != null && vibe.hasVibrator()) vibe.vibrate(200);
    }

    public void ringtone() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(this, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
