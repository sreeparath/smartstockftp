package com.dcode.SmartIM.ui.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.NavDeepLinkBuilder;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dcode.SmartIM.R;
import com.dcode.SmartIM.adapter.BinItemDetailsAdapter;
import com.dcode.SmartIM.adapter.BinStockAdapter;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.Objects;

public class binStockEditActivity extends BaseActivity {

    private String binNo;
    private RecyclerView rvItems;
    private BinItemDetailsAdapter viewAdapter;

    private NavController mNavController = null;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_bin_edit;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        setTitle(R.string.edit_bin);
        binNo = getIntent().getStringExtra("binNo");
        MaterialButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener((view -> {
//            super.onResume();


//                    finish();
            onBackPressed();

//            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//            startActivity(intent);
//            finish();
//            Fragment mFragment = null;
//            mFragment = new BinStockFragment();
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction().replace(R.id.binStockFragment, mFragment).commit();
            }));
        rvItems = findViewById(R.id.rvList);
        ((DefaultItemAnimator) Objects.requireNonNull(rvItems.getItemAnimator())).setSupportsChangeAnimations(false);
        rvItems.setLayoutManager(new LinearLayoutManager(binStockEditActivity.this));
        viewAdapter = new BinItemDetailsAdapter(new ArrayList<>());

        rvItems.setAdapter(viewAdapter);
        viewAdapter.getBinStockData(binNo);
        showToast(binNo);
        viewAdapter.notifyDataSetChanged();


        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {

                shortVibration();
                AlertDialog.Builder alert = new AlertDialog.Builder(binStockEditActivity.this);
                alert.setCancelable(false);
                alert.setTitle("Delete Bin ?");
                alert.setPositiveButton("Ok", (dialog, which) -> {
                    showToast("Deleted");
                    int position = viewHolder.getAdapterPosition();
                    viewAdapter.removeItem(position);
                    viewAdapter.notifyDataSetChanged();

                });
                alert.setNegativeButton("Cancel", (dialog, which) -> {
                    rvItems.setAdapter(viewAdapter);
                });
                alert.show();

            }
        };


        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(rvItems);
    }
}

