package com.dcode.SmartIM.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.dcode.SmartIM.App;
import com.dcode.SmartIM.R;
import com.dcode.SmartIM.common.AppVariables;
import com.dcode.SmartIM.common.CryptoSecurity;
import com.dcode.SmartIM.common.PermissionsManager;
import com.dcode.SmartIM.data.MastersDBManager;
import com.dcode.SmartIM.data.model.FTP_SETTINGS;
import com.dcode.SmartIM.data.model.STOCK_SETTINGS;
import com.dcode.SmartIM.data.model.USERS;
import com.dcode.SmartIM.data.model.USER_SETTINGS;
import com.google.android.material.button.MaterialButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

public class LoginActivity extends BaseActivity {
    private EditText edUserName;
    private EditText edPassword;
    private EditText edDownloadDB;
    private TextView tvPdtId;
    private MaterialButton btDownload;
    private MastersDBManager mastersDBManager;


    private FTP_SETTINGS ftpSettings;
    private String fileNamePart;
    private String remoteFile;
    private String localFile;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {

        View.OnFocusChangeListener ofcListener = new KeyboardHideListener();

        edUserName = findViewById(R.id.edUserName);
        edUserName.setOnFocusChangeListener(ofcListener);

        edPassword = findViewById(R.id.edPassword);
        edPassword.setOnFocusChangeListener(ofcListener);

//        tvWorkSites = findViewById(R.id.acWorkSites);

        MaterialButton btnLogin = findViewById(R.id.btSignIn);
        btnLogin.setOnClickListener(v -> {
            try {
                OnClick_Login();
            } catch (Exception e) {
                Log.d(App.TAG, e.toString());
            }
        });

//        ImageButton settings = findViewById(R.id.imgSettings);

        btDownload = findViewById(R.id.btDownload);
        btDownload.setEnabled(false);
        btDownload.setOnClickListener(v -> DownloadMasterDataFile());

        edDownloadDB = findViewById(R.id.edDownloadDB);
        File masterDB = new File(AppVariables.getMastersDb());
        if (masterDB.exists()) {
            showToast("Master db present");
            edDownloadDB.setText("Master db present");
            edDownloadDB.setEnabled(false);
            }else{ edDownloadDB.setOnFocusChangeListener(ofcListener);
            edDownloadDB.setOnKeyListener((v, keyCode, event) -> {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        onDownloadDBKeyEvent();
                    }
                    return true;
                } else {
                    return false;
                }
            });}

        //edDownloadDB.setEnabled(false);

        PermissionsManager.verifyWriteStoragePermissions(this);
        PermissionsManager.verifyWriteStoragePermissions(this);

        tvPdtId = findViewById(R.id.tvPdtId);
        tvPdtId.setText(App.PDT_ID);

        mastersDBManager = new MastersDBManager(AppVariables.getMastersDb());

    }

    @Override
    public void onBackPressed() {
        finishAndRemoveTask();
        super.onBackPressed();
    }

    private void onDownloadDBKeyEvent() {
        try {
            String dnldBarcode = edDownloadDB.getText().toString();
            edDownloadDB.setText("");
            if (dnldBarcode.length() == 0) {
                return;
            }

            if (checkMasterDataBase(false)) {
                final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setIcon(R.drawable.dcode_black)
                        .setTitle("Existing data will be lost. Are you sure want to proceed ?")
                        .setPositiveButton("Yes",
                                (dialog, whichButton) -> {
                                    File file = new File(AppVariables.getMastersDb());
                                    file.delete();
                                    DecodeBarcode(dnldBarcode);
                                })
                        .setNegativeButton("No",
                                (dialog, whichButton) -> ftpSettings = null);
                alert.setCancelable(false);
                alert.show();
            } else {
                DecodeBarcode(dnldBarcode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void OnClick_Login() {
        try {
            if (!checkMasterDataBase(true))
                return;

            String uname = edUserName.getText().toString().trim();
            if (uname.isEmpty()) {
                edUserName.setError("User name required");
                showToast("User name required");
                return;
            }

            String uPass = edPassword.getText().toString().trim();
            if (uPass.isEmpty()) {
                edPassword.setError("Password required");
                showToast("Password required");
                return;
            }
            showProgress(false);
            if (ValidateUser(uname, uPass)) {
                getStockSettingData();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                showToast("Invalid Credentials!");
            }

        } catch (Exception e) {
            e.printStackTrace();
            showAlert("Error", e.getMessage(), true);
        }
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void DecodeBarcode(String barcode) {
        try {
            String decrypted = CryptoSecurity.decrypt(barcode);
            if (decrypted.length() == 0) {
                showAlert("Error", "Invalid  FTP barcode!", false);
                return;
            }

            JSONObject jsonObj = new JSONObject(decrypted);
            Log.d("jsonObj##", jsonObj.toString());
            if (jsonObj.has("FtpServer") && jsonObj.has("FtpPort") && jsonObj.has("Ftpuser")
                    && jsonObj.has("Ftppwd") && jsonObj.has("ftpfilename") &&
                    jsonObj.has("ftpFolder")) {

                ftpSettings = new FTP_SETTINGS();
                ftpSettings.ID = 9999;
                ftpSettings.HOST = jsonObj.get("FtpServer").toString();
                ftpSettings.PORT = Integer.parseInt(jsonObj.get("FtpPort").toString());
                ftpSettings.USER_NAME = jsonObj.get("Ftpuser").toString();
                ftpSettings.PASSWORD = jsonObj.get("Ftppwd").toString();

                fileNamePart = jsonObj.get("ftpfilename").toString();
                remoteFile = String.format("%s/%s", jsonObj.get("ftpFolder").toString(), fileNamePart);
                localFile = AppVariables.getMastersPath() + jsonObj.get("ftpfilename").toString();

                edDownloadDB.setText(ftpSettings.HOST);
                btDownload.setEnabled(true);
            } else {
                ftpSettings = null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void DownloadMasterDataFile() {
        try {
            if (ftpSettings == null || ftpSettings.ID <= 0) return;

            File dir = new File(AppVariables.getMastersPath());
            if (!dir.exists()) {
                dir.mkdirs();
            }

            btDownload.setEnabled(false);
            edDownloadDB.setText("Master db present");
            edDownloadDB.setEnabled(false);

            App.getDatabaseClient().getAppDatabase().genericDao().deleteAllBIN_ITEMS();

            showProgress(false);
            final boolean[] ftpStatus = {false};
            new Thread(){
                public void run() {
                    try {
                        FtpService ftpService = new FtpService();
                        final File lclFile = new File(localFile);
                        ftpStatus[0] = ftpService.DownloadFile(ftpSettings.HOST, ftpSettings.PORT, ftpSettings.USER_NAME, ftpSettings.PASSWORD, remoteFile, lclFile);

                        if (ftpStatus[0]==true) {
                            ftpService.UnpackMastersDbZip(AppVariables.getMastersPath(), fileNamePart, "Masters.db");
                            showProgress(false);


                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        showAlert("Error", e.getMessage(), false);
                    } finally {

                        dismissProgress();
                    }

                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
            showAlert("Error", "Invalid  FTP barcode!", false);
        }
    }

    private boolean checkMasterDataBase(boolean showAlert) {
        try {
            File masterDB = new File(AppVariables.getMastersDb());
            if (!masterDB.exists()) {
                if (showAlert) {
                    showAlert("Stop", "Master database not found.", false);
                }
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            showAlert("Error", e.getMessage(), true);
        }
        return true;
    }

    private void getStockSettingData() {
        STOCK_SETTINGS stock_settings = null;
        USER_SETTINGS user_settings = null;

        try {
            showProgress(true);
            if (mastersDBManager != null) {
                mastersDBManager.open();
                stock_settings = mastersDBManager.getStockSettings();
//                user_settings= mastersDBManager.getUserSettings();
                mastersDBManager.close();

            }
            if (stock_settings != null && stock_settings.DESCR.length() > 0) {
                AppVariables.CurrentStockSettings = stock_settings;
//                AppVariables.CurrentUserSettings=user_settings;
                tvPdtId.setText(String.format("%s - %s", stock_settings.STK_CODE, stock_settings.DESCR));
            } else {
                showAlert("Error", "Stock count settings not found!", true);
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showToast("Error accessing stock_settings");
        } finally {
            dismissProgress();
        }
    }


    private boolean ValidateUser(String uName, String uPass) {
        boolean status = false;
        USERS curUser = null;

        try {
            showProgress(true);
            if (mastersDBManager != null) {
                mastersDBManager.open();
                curUser = mastersDBManager.getUser(uName, uPass);
                mastersDBManager.close();
            }
            if (curUser != null && curUser.LOGIN_NAME.length() > 0) {
                AppVariables.CurrentUser = curUser;
                status = true;
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
            showToast("Error accessing Users Master");
        } finally {
            dismissProgress();
        }

        return status;
    }

    private class KeyboardHideListener implements View.OnFocusChangeListener {

        public void onFocusChange(View v, boolean hasFocus) {

            if (v.getId() == R.id.edPassword && !hasFocus) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            }
        }
    }
}
