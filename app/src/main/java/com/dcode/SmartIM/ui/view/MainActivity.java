package com.dcode.SmartIM.ui.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.dcode.SmartIM.App;
import com.dcode.SmartIM.R;
import com.dcode.SmartIM.common.AppVariables;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends BaseActivity {
    private AppBarConfiguration mAppBarConfiguration;
    private NavController mNavController = null;


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState) {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TextView tvCurrentUser = findViewById(R.id.tvCurrentUser);
        tvCurrentUser.setText(String.format("User: %s", AppVariables.CurrentUser.LOGIN_NAME));

        if (savedInstanceState == null) {
            setupDrawerNavigationBar();
        }
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        setupDrawerNavigationBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.top_app_bar, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(mNavController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void setupDrawerNavigationBar() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_side_drawer_main);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_logout)
                .setDrawerLayout(drawer)
                .build();
        mNavController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, mNavController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, mNavController);
    }

    public void SignOut() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    public void NavigateToFragment(int fragmentID, @Nullable Bundle bundle) {
        if (bundle == null) {
            mNavController.navigate(fragmentID);
        } else {
            mNavController.navigate(fragmentID, bundle);
        }
    }

    public void NavigateBackStack(@Nullable Integer fragmentID, boolean inclusive) {
        try {
            if (fragmentID != null) {
                mNavController.popBackStack(fragmentID, inclusive);
            } else {
                mNavController.popBackStack();
            }
        } catch (Exception e) {
            Log.d(App.TAG, e.toString());
        }
    }
}
